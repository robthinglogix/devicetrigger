package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.thinglogix.Constants;

public class GetDeviceTypeServiceCall {

	public Map<String, Object> getDeviceType(GetDeviceTypeLambdaInput input, AWSLambda lambda) {
		Map<String, Object> device = new HashMap<String, Object>();
		GetDeviceTypeService service = LambdaInvokerFactory.builder().lambdaClient(lambda)
				.build(GetDeviceTypeService.class);
		if (Constants.envType.equalsIgnoreCase("Dev"))
			device = (Map<String, Object>) service.getDeviceTypeDev(input);
		if (Constants.envType.equalsIgnoreCase("Develop"))
			device = (Map<String, Object>) service.getDeviceTypeDevelop(input);
		if (Constants.envType.equalsIgnoreCase("Stage"))
			device = (Map<String, Object>) service.getDeviceTypeStage(input);
		if (Constants.envType.equalsIgnoreCase("Prod"))
			device = (Map<String, Object>) service.getDeviceTypeProd(input);

		return device;
	}

}
