package com.thinglogix.lambda;

import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface SearchService {
	@LambdaFunction(functionName=LambdaFunctionNames.POST_SEARCH_SERVICE)
	List<Map<String, Object>> searchDeviceDev(SearchServiceInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.POST_SEARCH_SERVICE_DEVELOP)
	List<Map<String, Object>> searchDeviceDevelop(SearchServiceInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.POST_SEARCH_SERVICE_STAGE)
	List<Map<String, Object>> searchDeviceStage(SearchServiceInput input);
	
	
	@LambdaFunction(functionName=LambdaFunctionNames.POST_SEARCH_SERVICE_PROD)
	List<Map<String, Object>> searchDeviceProd(SearchServiceInput input);
}
