package com.thinglogix.lambda;

import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface GetDeviceChildrenService {
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_CHILDREN)
	List<Map<String, Object>> getDeviceChildrenDev(GetDeviceChildrenInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_CHILDREN_DEVELOP)
	List<Map<String, Object>> getDeviceChildrenDevelop(GetDeviceChildrenInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_CHILDREN_STAGE)
	List<Map<String, Object>> getDeviceChildrenStage(GetDeviceChildrenInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_CHILDREN_PROD)
	List<Map<String, Object>> getDeviceChildrenProd(GetDeviceChildrenInput input);
}
