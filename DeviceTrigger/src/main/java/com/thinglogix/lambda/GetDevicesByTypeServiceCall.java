package com.thinglogix.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.thinglogix.Constants;

public class GetDevicesByTypeServiceCall {

	 public List<Map<String, Object>> getDeviceHistory(GetDevicesByTypeLambdaInput input,AWSLambda lambda){
		 List<Map<String, Object>>  device =new ArrayList<Map<String, Object>>();
		 GetDevicesByTypeService service = LambdaInvokerFactory.builder().lambdaClient(lambda)
					.build(GetDevicesByTypeService.class);
		 	if(Constants.envType.equalsIgnoreCase("Dev"))
				device = (List<Map<String, Object>>) service.getDeviceTypeDev(input);
		 	if(Constants.envType.equalsIgnoreCase("Develop"))
				device = (List<Map<String, Object>>) service.getDeviceTypeDevelop(input);
			if(Constants.envType.equalsIgnoreCase("Stage"))
				device = (List<Map<String, Object>>) service.getDeviceTypeStage(input);
			if(Constants.envType.equalsIgnoreCase("Prod"))
				device = (List<Map<String, Object>>) service.getDeviceTypeProd(input);
			
			return device;
	 }
	 
}
