package com.thinglogix.lambda;

import java.util.Map;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface GetUserDetailsService {
	@LambdaFunction(functionName=LambdaFunctionNames.GET_USER_DETAILS)
	Map<String, Object> getUserDetailsDev(GetUserDetailsInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_USER_DETAILS_DEVELOP)
	Map<String, Object> getUserDetailsDevelop(GetUserDetailsInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_USER_DETAILS_STAGE)
	Map<String, Object> getUserDetailsStage(GetUserDetailsInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_USER_DETAILS_PROD)
	Map<String, Object> getUserDetailsProd(GetUserDetailsInput input);
}
