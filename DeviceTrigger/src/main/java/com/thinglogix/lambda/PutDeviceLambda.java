package com.thinglogix.lambda;

import java.util.Map;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface PutDeviceLambda {
	@LambdaFunction(functionName=LambdaFunctionNames.PUT_DEVICE)
	Map<String, Object> putDeviceDev(PutDeviceLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.PUT_DEVICE_DEVELOP)
	Map<String, Object> putDeviceDevelop(PutDeviceLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.PUT_DEVICE_STAGE)
	Map<String, Object> putDeviceStage(PutDeviceLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.PUT_DEVICE_PROD)
	Map<String, Object> putDeviceProd(PutDeviceLambdaInput input);
}
