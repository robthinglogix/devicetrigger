package com.thinglogix.lambda;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.thinglogix.Constants;

public class PostActionServiceCall {

	public Object getDevice(PostActionInput input, AWSLambda lambda) {
		Object device = new Object();
		PostActionService service = LambdaInvokerFactory.builder().lambdaClient(lambda).build(PostActionService.class);

		if (Constants.envType.equalsIgnoreCase("Dev"))
			device = (Object) service.getDeviceDev(input);
		if (Constants.envType.equalsIgnoreCase("Develop"))
			device = (Object) service.getDeviceDevelop(input);
		if (Constants.envType.equalsIgnoreCase("Stage"))
			device = (Object) service.getDeviceStage(input);
		if (Constants.envType.equalsIgnoreCase("Prod"))
			device = (Object) service.getDeviceProd(input);

		return device;
	}

}
