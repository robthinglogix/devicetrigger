package com.thinglogix.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.thinglogix.Constants;

public class GetDeviceChildrenServiceCall {
	public List<Map<String, Object>> getDeviceChildren(GetDeviceChildrenInput input,AWSLambda lambda){
		List<Map<String, Object>>  device =new ArrayList<Map<String, Object>>();
		GetDeviceChildrenService service = LambdaInvokerFactory.builder().lambdaClient(lambda)
				.build(GetDeviceChildrenService.class);
			
		 	if(Constants.envType.equalsIgnoreCase("Dev"))
				device = (List<Map<String, Object>>) service.getDeviceChildrenDev(input);
		 	if(Constants.envType.equalsIgnoreCase("Develop"))
				device = (List<Map<String, Object>>) service.getDeviceChildrenDevelop(input);
			if(Constants.envType.equalsIgnoreCase("Stage"))
				device = (List<Map<String, Object>>) service.getDeviceChildrenStage(input);
			if(Constants.envType.equalsIgnoreCase("Prod"))
				device = (List<Map<String, Object>>) service.getDeviceChildrenProd(input);
			
			return device;
	 }
}
