package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

public class GetDeviceChildrenInput {
	private Map<String, String> params;

	private boolean invoke = true;
	public boolean isInvoke() {
		return invoke;
	}

	public void setInvoke(boolean invoke) {
		this.invoke = invoke;
	}

	public Map<String, String> getParams() {
		return params;
	}
	
	public void setObjectId(String objectID) {
		if(params == null) {
			params = new HashMap<String, String>();
		}
		params.put("device_id", objectID);
	}
	
	public String getObjectId() {
		if(params == null) {
			return null;
		}
		return params.get("device_id");
	}
}
