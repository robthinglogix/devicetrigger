package com.thinglogix.lambda;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface PostActionService {
	@LambdaFunction(functionName=LambdaFunctionNames.POST_ACTION_CALL)
	Object getDeviceDev(PostActionInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.POST_ACTION_CALL_DEVELOP)
	Object getDeviceDevelop(PostActionInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.POST_ACTION_CALL_STAGE)
	Object getDeviceStage(PostActionInput input);
	
	
	@LambdaFunction(functionName=LambdaFunctionNames.POST_ACTION_CALL_PROD)
	Object getDeviceProd(PostActionInput input);
}
