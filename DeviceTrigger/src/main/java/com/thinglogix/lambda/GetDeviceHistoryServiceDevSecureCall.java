package com.thinglogix.lambda;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.thinglogix.Constants;

public class GetDeviceHistoryServiceDevSecureCall {
	 public List<Map<String, Object>> getDeviceHistory(GetDeviceHistoryDevSecureLambdaInput input,AWSLambda lambda){
		 List<Map<String, Object>>  device =new ArrayList<Map<String, Object>>();
		 GetDeviceHistoryServiceDevSecure service = LambdaInvokerFactory.builder().lambdaClient(lambda)
					.build(GetDeviceHistoryServiceDevSecure.class);
			
		 	if(Constants.envType.equalsIgnoreCase("Dev"))
				device = (List<Map<String, Object>>) service.getDeviceHistoryDev(input);
		 	if(Constants.envType.equalsIgnoreCase("Develop"))
				device = (List<Map<String, Object>>) service.getDeviceHistoryDevelop(input);
			if(Constants.envType.equalsIgnoreCase("Stage"))
				device = (List<Map<String, Object>>) service.getDeviceHistoryStage(input);
			if(Constants.envType.equalsIgnoreCase("Prod"))
				device = (List<Map<String, Object>>) service.getDeviceHistoryProd(input);
			
			return device;
	 }
}
