package com.thinglogix.lambda;

public class LambdaFunctionNames {
	public static final String 			GET_DEVICE_TYPE = "fndy-get-device-type-secure-dev";
	public static final String  	GET_DEVICES_BY_TYPE = "fndy-get-devices-by-type-secure-dev";
	public static final String 			 	 GET_DEVICE = "fndy-get-device-secure-dev";
	public static final String 	  	  POST_PUBLISH_MQTT = "fndy-post-publish-mqtt-secure-dev";
	public static final String GET_OBJECT_REFERENCES_TO = "fndy-get-object-references-to-secure-dev";
	public static final String 		   POST_ACTION_CALL = "fndy-post-action-call-secure-dev";
	public static final String 				POST_DEVICE = "fndy-post-device-secure-dev";
	public static final String 				 PUT_DEVICE = "fndy-put-device-secure-dev";
	public static final String 			  POST_JUNCTION = "fndy-post-junction-secure-dev";
	public static final String 		POST_SEARCH_SERVICE = "fndy-post-search-service-secure-dev";
	public static final String 		 GET_DEVICE_HISTORY = "fndy-get-object-history-secure-dev";
	public static final String		GET_DEVICE_PARENTS= "fndy-get-device-parents-secure-dev";
	public static final String 		GET_USER_DETAILS = "fndy-get-user-secure-dev";
	public static final String 		GET_ACCOUNT_DETAILS = "fndy-get-account-secure-dev";
	public static final String 		GET_DEVICE_CHILDREN = "fndy-get-device-children-secure-dev";
	
	public static final String 			GET_DEVICE_TYPE_DEVELOP = "fndy-get-device-type-secure-develop";
	public static final String  	GET_DEVICES_BY_TYPE_DEVELOP = "fndy-get-devices-by-type-secure-develop";
	public static final String 			 	 GET_DEVICE_DEVELOP = "fndy-get-device-secure-develop";
	public static final String 	  	  POST_PUBLISH_MQTT_DEVELOP = "fndy-post-publish-mqtt-secure-develop";
	public static final String GET_OBJECT_REFERENCES_TO_DEVELOP = "fndy-get-object-references-to-secure-develop";
	public static final String 		   POST_ACTION_CALL_DEVELOP = "fndy-post-action-call-secure-develop";
	public static final String 				POST_DEVICE_DEVELOP = "fndy-post-device-secure-develop";
	public static final String 				 PUT_DEVICE_DEVELOP = "fndy-put-device-secure-develop";
	public static final String 			  POST_JUNCTION_DEVELOP = "fndy-post-junction-secure-develop";
	public static final String 		POST_SEARCH_SERVICE_DEVELOP = "fndy-post-search-service-secure-develop";
	public static final String 		 GET_DEVICE_HISTORY_DEVELOP = "fndy-get-object-history-secure-develop";
	public static final String		GET_DEVICE_PARENTS_DEVELOP = "fndy-get-device-parents-secure-develop";
	public static final String 		GET_USER_DETAILS_DEVELOP = "fndy-get-user-secure-develop";
	public static final String 		GET_ACCOUNT_DETAILS_DEVELOP = "fndy-get-account-secure-develop";
	public static final String 		GET_DEVICE_CHILDREN_DEVELOP = "fndy-get-device-children-secure-develop";
	
	public static final String 			GET_DEVICE_TYPE_STAGE = "fndy-get-device-type-secure-stage";
	public static final String  	GET_DEVICES_BY_TYPE_STAGE = "fndy-get-devices-by-type-secure-stage";
	public static final String 			 	 GET_DEVICE_STAGE = "fndy-get-device-secure-stage";
	public static final String 	  	  POST_PUBLISH_MQTT_STAGE = "fndy-post-publish-mqtt-secure-stage";
	public static final String GET_OBJECT_REFERENCES_TO_STAGE = "fndy-get-object-references-to-secure-stage";
	public static final String 		   POST_ACTION_CALL_STAGE = "fndy-post-action-call-secure-stage";
	public static final String 				POST_DEVICE_STAGE = "fndy-post-device-secure-stage";
	public static final String 				 PUT_DEVICE_STAGE = "fndy-put-device-secure-stage";
	public static final String 			  POST_JUNCTION_STAGE = "fndy-post-junction-secure-stage";
	public static final String 		POST_SEARCH_SERVICE_STAGE = "fndy-post-search-service-secure-stage";
	public static final String 		 GET_DEVICE_HISTORY_STAGE = "fndy-get-object-history-secure-stage";
	public static final String		GET_DEVICE_PARENTS_STAGE= "fndy-get-device-parents-secure-stage";
	public static final String 		GET_USER_DETAILS_STAGE = "fndy-get-user-secure-stage";
	public static final String 		GET_ACCOUNT_DETAILS_STAGE = "fndy-get-account-secure-stage";
	public static final String 		GET_DEVICE_CHILDREN_STAGE = "fndy-get-device-children-secure-stage";
	
	public static final String 			GET_DEVICE_TYPE_PROD = "fndy-get-device-type-secure-prod";
	public static final String  	GET_DEVICES_BY_TYPE_PROD = "fndy-get-devices-by-type-secure-prod";
	public static final String 			 	 GET_DEVICE_PROD = "fndy-get-device-secure-prod";
	public static final String 	  	  POST_PUBLISH_MQTT_PROD = "fndy-post-publish-mqtt-secure-prod";
	public static final String GET_OBJECT_REFERENCES_TO_PROD = "fndy-get-object-references-to-secure-prod";
	public static final String 		   POST_ACTION_CALL_PROD = "fndy-post-action-call-secure-prod";
	public static final String 				POST_DEVICE_PROD = "fndy-post-device-secure-prod";
	public static final String 				 PUT_DEVICE_PROD = "fndy-put-device-secure-prod";
	public static final String 			  POST_JUNCTION_PROD = "fndy-post-junction-secure-prod";
	public static final String 		POST_SEARCH_SERVICE_PROD = "fndy-post-search-service-secure-prod";
	public static final String 		 GET_DEVICE_HISTORY_PROD = "fndy-get-object-history-secure-prod";
	public static final String		GET_DEVICE_PARENTS_PROD= "fndy-get-device-parents-secure-prod";
	public static final String 		GET_USER_DETAILS_PROD = "fndy-get-user-secure-prod";
	public static final String 		GET_ACCOUNT_DETAILS_PROD = "fndy-get-account-secure-prod";
	public static final String 		GET_DEVICE_CHILDREN_PROD = "fndy-get-device-children-secure-prod";

}
