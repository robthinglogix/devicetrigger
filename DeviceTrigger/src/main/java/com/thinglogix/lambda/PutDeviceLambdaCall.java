package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.thinglogix.Constants;

public class PutDeviceLambdaCall {
	public Map<String, Object> putDevice(PutDeviceLambdaInput input, AWSLambda lambda) {
		Map<String, Object> device = new HashMap<String, Object>();
		PutDeviceLambda service = LambdaInvokerFactory.builder().lambdaClient(lambda).build(PutDeviceLambda.class);

		if (Constants.envType.equalsIgnoreCase("Dev"))
			device = (Map<String, Object>) service.putDeviceDev(input);
		if (Constants.envType.equalsIgnoreCase("Develop"))
			device = (Map<String, Object>) service.putDeviceDevelop(input);
		if (Constants.envType.equalsIgnoreCase("Stage"))
			device = (Map<String, Object>) service.putDeviceStage(input);
		if (Constants.envType.equalsIgnoreCase("Prod"))
			device = (Map<String, Object>) service.putDeviceProd(input);

		return device;
	}
}
