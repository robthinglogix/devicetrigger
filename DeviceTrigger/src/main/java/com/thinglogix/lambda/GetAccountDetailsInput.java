package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

public class GetAccountDetailsInput {
	private Map<String, String> params;
	private Map<String,String> query;
	private boolean invoke = true;
	
	public boolean isInvoke() {
		return invoke;
	}

	public void setInvoke(boolean invoke) {
		this.invoke = invoke;
	}
	public Map<String, String> getQuery() {
		return query;
	}

	public void setQuery(Map<String, String> query) {
		this.query = query;
	}
	public Map<String, String> getParams() {
		return params;
	}
	
	public void setAccountId(String account_id) {
		if(params == null) {
			params = new HashMap<String, String>();
		}
		params.put("account_id", account_id);
	}
	
	
	public String getAccountId() {
		if(params == null) {
			return null;
		}
		return params.get("account_id");
	}
}
