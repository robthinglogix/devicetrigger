package com.thinglogix.lambda;

import java.util.Map;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface MqttPublishService {
	@LambdaFunction(functionName=LambdaFunctionNames.POST_PUBLISH_MQTT)
	Map<String, Object> publishMqttDev(MqttPublishLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.POST_PUBLISH_MQTT_DEVELOP)
	Map<String, Object> publishMqttDevelop(MqttPublishLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.POST_PUBLISH_MQTT_STAGE)
	Map<String, Object> publishMqttStage(MqttPublishLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.POST_PUBLISH_MQTT_PROD)
	Map<String, Object> publishMqttProd(MqttPublishLambdaInput input);

}
