package com.thinglogix.lambda;

import java.util.Map;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

 public interface GetDeviceService {
	 
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE)
	Map<String, Object> getDeviceDev(GetDeviceLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_DEVELOP)
	Map<String, Object> getDeviceDevelop(GetDeviceLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_STAGE)
	Map<String, Object> getDeviceStage(GetDeviceLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_PROD)
	Map<String, Object> getDeviceProd(GetDeviceLambdaInput input);
}
