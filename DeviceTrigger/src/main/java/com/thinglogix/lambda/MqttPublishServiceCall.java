package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.thinglogix.Constants;

public class MqttPublishServiceCall {
	public Map<String, Object> publishMqtt(MqttPublishLambdaInput input, AWSLambda lambda) {
		Map<String, Object> device = new HashMap<String, Object>();
		MqttPublishService service = LambdaInvokerFactory.builder().lambdaClient(lambda)
				.build(MqttPublishService.class);

		if (Constants.envType.equalsIgnoreCase("Dev"))
			device = (Map<String, Object>) service.publishMqttDev(input);
		if (Constants.envType.equalsIgnoreCase("Develop"))
			device = (Map<String, Object>) service.publishMqttDevelop(input);
		if (Constants.envType.equalsIgnoreCase("Stage"))
			device = (Map<String, Object>) service.publishMqttStage(input);
		if (Constants.envType.equalsIgnoreCase("Prod"))
			device = (Map<String, Object>) service.publishMqttProd(input);

		return device;
	}
}
