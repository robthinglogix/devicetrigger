package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

public class PostActionInput {
	private Map<String, String> body;
	private boolean invoke = true;
	public void setActionId(String actionId) {
		if(body == null) {
			body = new HashMap<>();
		}
		body.put("actionId", actionId);
	}
	public void setObjectId(String objectId) {
		if(body == null) {
			body = new HashMap<>();
		}
		body.put("objectId", objectId);
	}
	public Map<String, String> getBody() {
		return body;
	}
	public void setBody(Map<String, String> body) {
		this.body = body;
	}
	public boolean isInvoke() {
		return invoke;
	}
	public void setInvoke(boolean invoke) {
		this.invoke = invoke;
	}
}
