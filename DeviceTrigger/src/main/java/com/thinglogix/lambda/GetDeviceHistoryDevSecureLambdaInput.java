package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

public class GetDeviceHistoryDevSecureLambdaInput {
	private Map<String, Object> query;
	
	private boolean invoke = true;
	public boolean isInvoke() {
		return invoke;
	}

	public void setInvoke(boolean invoke) {
		this.invoke = invoke;
	}

	public Map<String, Object> getQuery() {
		return query;
	}

	public void setQuery(Map<String, Object> query) {
		this.query = query;
	}

	public void setDeviceId(String deviceId) {
		if(query == null) {
			query = new HashMap<String, Object>();
		}
		query.put("device_id", deviceId);
	}

	public void setAttribute(String attr) {
		if(query == null) {
			query = new HashMap<String, Object>();
		}
		query.put("fields", attr);
	}

	public void setEndTime(String endTime) {
		if(query == null) {
			query = new HashMap<String, Object>();
		}
		query.put("end", endTime);
	}

	public void setStartTime(String startTime) {
		if(query == null) {
			query = new HashMap<String, Object>();
		}
		query.put("start", startTime);
	}

	public void setLimit(String limit) {
		if(query == null) {
			query = new HashMap<String, Object>();
		}
		query.put("limit", limit);
	}
	
	
}
