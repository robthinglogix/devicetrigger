package com.thinglogix.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.thinglogix.Constants;

public class GetDeviceParentsServiceCall {
	public List<Map<String, Object>> getDeviceParents(GetDeviceParentsInput input,AWSLambda lambda){
		List<Map<String, Object>>  device =new ArrayList<Map<String, Object>>();
		GetDeviceParentsService service = LambdaInvokerFactory.builder().lambdaClient(lambda)
				.build(GetDeviceParentsService.class);
			if(Constants.envType.equalsIgnoreCase("Dev"))
				device = (List<Map<String, Object>>) service.getDeviceParentsDev(input);
		 	if(Constants.envType.equalsIgnoreCase("Develop"))
				device = (List<Map<String, Object>>) service.getDeviceParentsDevelop(input);
			if(Constants.envType.equalsIgnoreCase("Stage"))
				device = (List<Map<String, Object>>) service.getDeviceParentsStage(input);
			if(Constants.envType.equalsIgnoreCase("Prod"))
				device = (List<Map<String, Object>>) service.getDeviceParentsProd(input);
			
			return device;
	 }
}
