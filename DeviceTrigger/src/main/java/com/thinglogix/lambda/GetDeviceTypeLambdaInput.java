package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

public class GetDeviceTypeLambdaInput {
	private Map<String, String> params;

	private boolean invoke = true;
	public boolean isInvoke() {
		return invoke;
	}

	public void setInvoke(boolean invoke) {
		this.invoke = invoke;
	}

	public Map<String, String> getParams() {
		return params;
	}
	
	public void setTypeId(String typeID) {
		if(params == null) {
			params = new HashMap<String, String>();
		}
		params.put("type_id", typeID);
	}
	
	public String getTypeId() {
		if(params == null) {
			return null;
		}
		return params.get("type_id");
	}
}
