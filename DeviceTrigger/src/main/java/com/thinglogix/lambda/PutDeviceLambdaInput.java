package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

public class PutDeviceLambdaInput {
	private Map<String, Object> body;
	private boolean invoke = true;
	public Map<String, Object> getBody() {
		return body;
	}
	public void setBody(Map<String, Object> body) {
		this.body = body;
	}
	public boolean isInvoke() {
		return invoke;
	}
	public void setInvoke(boolean invoke) {
		this.invoke = invoke;
	}
	
	
	
	public void setDeviceId(String deviceId) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("deviceId", deviceId);
		
	}
	
	public void setStatus(String status) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("a.status", status);
	}
	
	
	public void setLastPingTime(long lastPingDateTime) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("a.last_ping_date_time", lastPingDateTime);
	}
	
}
