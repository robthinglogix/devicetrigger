package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

public class MqttPublishLambdaInput {

	private Map<String, String> body;
	private boolean invoke = true;
	
	public Map<String, String> getbody() {
		return body;
	}
	
	public boolean isInvoke() {
		return invoke;
	}

	public void setInvoke(boolean invoke) {
		this.invoke = invoke;
	}
	
	public void setPayload(String payload) {
		if(body == null) {
			body = new HashMap<String, String>();
		}
		body.put("payload", payload);
	}
	
	public String getPayload() {
		if(body == null) {
			return null;
		}
		return body.get("payload");
	}
	
	
	public void setTopic(String topic) {
		if(body == null) {
			body = new HashMap<String, String>();
		}
		body.put("topic", topic);
	}
	
	public String getTopic() {
		if(body == null) {
			return null;
		}
		return body.get("topic");
	}

}
