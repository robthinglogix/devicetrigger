package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

public class GetDevicesByTypeLambdaInput {
	private Map<String, String> query;

	private boolean invoke = true;
	public boolean isInvoke() {
		return invoke;
	}

	public void setInvoke(boolean invoke) {
		this.invoke = invoke;
	}

	public Map<String, String> getQuery() {
		return query;
	}
	
	public void setTypeId(String typeID) {
		if(query == null) {
			query = new HashMap<String, String>();
		}
		query.put("type_id", typeID);
	}
	
	public String getTypeId() {
		if(query == null) {
			return null;
		}
		return query.get("type_id");
	}
}
