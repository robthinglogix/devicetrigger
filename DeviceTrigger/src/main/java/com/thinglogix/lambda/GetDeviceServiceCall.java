package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.thinglogix.Constants;

public class GetDeviceServiceCall {
	 public Map<String, Object> getDevice(GetDeviceLambdaInput input,AWSLambda lambda){
		 Map<String, Object> device =new HashMap<String, Object>();
		 GetDeviceService service = LambdaInvokerFactory.builder().lambdaClient(lambda)
					.build(GetDeviceService.class);
			
		 	if(Constants.envType.equalsIgnoreCase("Dev"))
				device = (Map<String, Object>) service.getDeviceDev(input);
		 	if(Constants.envType.equalsIgnoreCase("Develop"))
				device = (Map<String, Object>) service.getDeviceDevelop(input);
			if(Constants.envType.equalsIgnoreCase("Stage"))
				device = (Map<String, Object>) service.getDeviceStage(input);
			if(Constants.envType.equalsIgnoreCase("Prod"))
				device = (Map<String, Object>) service.getDeviceProd(input);
			
			return device;
	 }
}
