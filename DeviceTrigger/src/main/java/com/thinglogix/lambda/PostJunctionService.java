package com.thinglogix.lambda;

import java.util.Map;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface PostJunctionService {
	@LambdaFunction(functionName=LambdaFunctionNames.POST_JUNCTION)
	Map<String, Object> postDeviceDev(PostJunctionLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.POST_JUNCTION_DEVELOP)
	Map<String, Object> postDeviceDevelop(PostJunctionLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.POST_JUNCTION_STAGE)
	Map<String, Object> postDeviceStage(PostJunctionLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.POST_JUNCTION_PROD)
	Map<String, Object> postDeviceProd(PostJunctionLambdaInput input);
}
