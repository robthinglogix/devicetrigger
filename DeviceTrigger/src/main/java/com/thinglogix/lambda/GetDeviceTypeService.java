package com.thinglogix.lambda;

import java.util.Map;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface GetDeviceTypeService {
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_TYPE)
	Map<String, Object> getDeviceTypeDev(GetDeviceTypeLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_TYPE_DEVELOP)
	Map<String, Object> getDeviceTypeDevelop(GetDeviceTypeLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_TYPE_STAGE)
	Map<String, Object> getDeviceTypeStage(GetDeviceTypeLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_TYPE_PROD)
	Map<String, Object> getDeviceTypeProd(GetDeviceTypeLambdaInput input);
	
	
}

