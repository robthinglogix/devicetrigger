package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

public class GetUserDetailsInput {
	private Map<String, String> params;
	private Map<String,Boolean> query;
	

	public void setWithObjects(boolean withObjects) {
		if(query == null) {
			query = new HashMap<String, Boolean>();
		}
		query.put("withObjects", withObjects);
	}

	private boolean invoke = true;
	
	public boolean isInvoke() {
		return invoke;
	}

	public void setInvoke(boolean invoke) {
		this.invoke = invoke;
	}

	public Map<String, String> getParams() {
		return params;
	}
	
	public Map<String, Boolean> getQuery() {
		return query;
	}

	public void setQuery(Map<String, Boolean> query) {
		this.query = query;
	}

	public void setUserId(String user_id) {
		if(params == null) {
			params = new HashMap<String, String>();
		}
		params.put("user_id", user_id);
	}
	
	
	public String getUserId() {
		if(params == null) {
			return null;
		}
		return params.get("user_id");
	}
}
