package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.thinglogix.Constants;

public class GetUserDetailsServiceCall {
	public Map<String, Object> getUserDetails(GetUserDetailsInput input,AWSLambda lambda){
		Map<String, Object> user = new HashMap<String, Object>();
		GetUserDetailsService service = LambdaInvokerFactory.builder().lambdaClient(lambda)
				.build(GetUserDetailsService.class);
			
		 	if(Constants.envType.equalsIgnoreCase("Dev"))
				user = (Map<String, Object>) service.getUserDetailsDev(input);
		 	if(Constants.envType.equalsIgnoreCase("Develop"))
				user = (Map<String, Object>) service.getUserDetailsDevelop(input);
			if(Constants.envType.equalsIgnoreCase("Stage"))
				user = (Map<String, Object>) service.getUserDetailsStage(input);
			if(Constants.envType.equalsIgnoreCase("Prod"))
				user = (Map<String, Object>) service.getUserDetailsProd(input);
			
			return user;
	 }
}
