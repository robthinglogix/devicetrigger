package com.thinglogix.lambda;

import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface GetDeviceParentsService {
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_PARENTS)
	List<Map<String, Object>> getDeviceParentsDev(GetDeviceParentsInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_PARENTS_DEVELOP)
	List<Map<String, Object>> getDeviceParentsDevelop(GetDeviceParentsInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_PARENTS_STAGE)
	List<Map<String, Object>> getDeviceParentsStage(GetDeviceParentsInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_PARENTS_PROD)
	List<Map<String, Object>> getDeviceParentsProd(GetDeviceParentsInput input);
}
