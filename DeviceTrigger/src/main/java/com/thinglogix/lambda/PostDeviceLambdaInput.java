package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

public class PostDeviceLambdaInput {
	private Map<String, Object> body;
	private boolean invoke = true;
	
	//private boolean isParent;
	

	public Map<String, Object> getbody() {
		return body;
	}
	
	public void setAccountId(String accountId) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("accountId", accountId);
	}
	
	public String getAccountId() {
		if(body == null) {
			return null;
		}
		return (String) body.get("accountId");
	}
	
	public void setTypeId(String typeId) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("typeId", typeId);
	}
	
	public String getTypeId() {
		if(body == null) {
			return null;
		}
		return (String) body.get("typeId");
	}
	
	public void setMqtt_topic(String mqtt_topic) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("a.mqtt_topic", mqtt_topic);
	}
	
	public String getMqtt_topic() {
		if(body == null) {
			return null;
		}
		return (String) body.get("a.mqtt_topic");
	}
	
	public void setName(String name) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("a.name", name);
	}
	
	public String getName() {
		if(body == null) {
			return null;
		}
		return (String) body.get("a.name");
	}
	
	public boolean isInvoke() {
		return invoke;
	}

	public void setInvoke(boolean invoke) {
		this.invoke = invoke;
	}

	public void setDeviceId(String deviceId) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("deviceId", deviceId);
		
	}

	public void setModumType(String modumType) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("a.modum_type", modumType);
	}
	
	public String geModumType() {
		if(body == null) {
			return null;
		}
		return (String) body.get("a.modum_type");
	}
	
	
	public void setDescription(String description) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("a.description", description);
	}
	
	public String getDescription() {
		if(body == null) {
			return null;
		}
		return (String) body.get("a.description");
	}
	
	
	public void setPumpCount(String pumpCount) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("a.pumpCount", pumpCount);
	}
	
	public String getPumpCount() {
		if(body == null) {
			return null;
		}
		return (String) body.get("a.pumpCount");
	}
	
	public void setRssi(String rssi) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("a.rssi", rssi);
	}
	
	public String getRssi() {
		if(body == null) {
			return null;
		}
		return (String) body.get("a.rssi");
	}
	
	
	
	public void setDataItemName(String dataItemName) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("a.data_item_name", dataItemName);
	}
	
	public String getDataItemName() {
		if(body == null) {
			return null;
		}
		return (String) body.get("a.data_item_name");
	}
	
	
	public void setDataItemValue(String dataItemValue) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("a.data_item_value", dataItemValue);
	}
	
	public String getDataItemValue() {
		if(body == null) {
			return null;
		}
		return (String) body.get("a.data_item_value");
	}
	
	
	public void setSeverity(Integer severity) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("a.severity", severity);
	}
	
	public Object getSeverity() {
		if(body == null) {
			return null;
		}
		return body.get("a.severity");
	}
	

	public void setRefId(String refKey,String refId) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("a."+refKey, refId);
	}
	
	public String getRefId(String refKey) {
		if(body == null) {
			return null;
		}
		return (String) body.get("a."+refKey);
	}
	
	public void setState(String state) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("a.state", state);
	}
	public void setBodyAttribute(String key,long alarmStartTime) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put(key, alarmStartTime);
	}
	
}
