package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

public class SearchServiceInput {
	private Map<String, String> body;
	private boolean invoke = true;

	public Map<String, String> getParams() {
		return body;
	}
	
	public void setPhrase(String phrase) {
		if(body == null) {
			body = new HashMap<String, String>();
		}
		body.put("phrase", phrase);
	}
	
	public void setAccountId(String accountId) {
		if(body == null) {
			body = new HashMap<String, String>();
		}
		body.put("accountId", accountId);
	}

	public Map<String, String> getBody() {
		return body;
	}

	public void setBody(Map<String, String> body) {
		this.body = body;
	}

	public boolean isInvoke() {
		return invoke;
	}

	public void setInvoke(boolean invoke) {
		this.invoke = invoke;
	}
	
	
	
	
}
