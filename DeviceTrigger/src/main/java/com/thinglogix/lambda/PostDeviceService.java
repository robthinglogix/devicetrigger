package com.thinglogix.lambda;

import java.util.Map;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface PostDeviceService {
	@LambdaFunction(functionName=LambdaFunctionNames.POST_DEVICE)
	Map<String, Object> postDeviceDev(PostDeviceLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.POST_DEVICE_DEVELOP)
	Map<String, Object> postDeviceDevelop(PostDeviceLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.POST_DEVICE_STAGE)
	Map<String, Object> postDeviceStage(PostDeviceLambdaInput input);
	
	
	@LambdaFunction(functionName=LambdaFunctionNames.POST_DEVICE_PROD)
	Map<String, Object> postDeviceProd(PostDeviceLambdaInput input);
}
