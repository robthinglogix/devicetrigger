package com.thinglogix.lambda;

import java.util.Map;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface GetAccountDetailsService {
	@LambdaFunction(functionName=LambdaFunctionNames.GET_ACCOUNT_DETAILS)
	Map<String, Object> getAccountDetailsDev(GetAccountDetailsInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_ACCOUNT_DETAILS_DEVELOP)
	Map<String, Object> getAccountDetailsDevelop(GetAccountDetailsInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_ACCOUNT_DETAILS_STAGE)
	Map<String, Object> getAccountDetailsStage(GetAccountDetailsInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_ACCOUNT_DETAILS_PROD)
	Map<String, Object> getAccountDetailsProd(GetAccountDetailsInput input);
}
