package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

public class ObjectReferenceServiceInput {
	private Map<String, String> params;
	private Map<String, Object> query;
	private boolean invoke = true;
	
	public void setDeviceId(String deviceId) {
		if(params == null) {
			params = new HashMap<String, String>();
		}
		params.put("device_id", deviceId);
		
		if(query == null) {
			query = new HashMap<String, Object>();
		}
		query.put("with_types", true);
	}

	public Map<String, String> getParams() {
		return params;
	}

	public Map<String, Object> getQuery() {
		return query;
	}

	public void setQuery(Map<String, Object> query) {
		this.query = query;
	}

	public boolean isInvoke() {
		return invoke;
	}
	
	
}
