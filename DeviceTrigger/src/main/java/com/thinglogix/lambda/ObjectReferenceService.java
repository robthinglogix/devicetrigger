package com.thinglogix.lambda;

import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface ObjectReferenceService {
	@LambdaFunction(functionName=LambdaFunctionNames.GET_OBJECT_REFERENCES_TO)
	Map<String, List<Map<String, Object>>> findReferencesDev(ObjectReferenceServiceInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_OBJECT_REFERENCES_TO_DEVELOP)
	Map<String, List<Map<String, Object>>> findReferencesDevelop(ObjectReferenceServiceInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_OBJECT_REFERENCES_TO_STAGE)
	Map<String, List<Map<String, Object>>> findReferencesStage(ObjectReferenceServiceInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_OBJECT_REFERENCES_TO_PROD)
	Map<String, List<Map<String, Object>>> findReferencesProd(ObjectReferenceServiceInput input);
}
