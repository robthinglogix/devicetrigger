package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.thinglogix.Constants;

public class GetAccountDetailsServiceCall {
	public Map<String, Object> getAccountDetails(GetAccountDetailsInput input,AWSLambda lambda){
		Map<String, Object> user = new HashMap<String, Object>();
		GetAccountDetailsService service = LambdaInvokerFactory.builder().lambdaClient(lambda)
				.build(GetAccountDetailsService.class);
			
		 	if(Constants.envType.equalsIgnoreCase("Dev"))
				user = (Map<String, Object>) service.getAccountDetailsDev(input);
		 	if(Constants.envType.equalsIgnoreCase("Develop"))
				user = (Map<String, Object>) service.getAccountDetailsDevelop(input);
			if(Constants.envType.equalsIgnoreCase("Stage"))
				user = (Map<String, Object>) service.getAccountDetailsStage(input);
			if(Constants.envType.equalsIgnoreCase("Prod"))
				user = (Map<String, Object>) service.getAccountDetailsProd(input);
			
			return user;
	 }
}
