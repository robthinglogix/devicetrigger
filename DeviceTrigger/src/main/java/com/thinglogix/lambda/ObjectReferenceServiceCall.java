package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.thinglogix.Constants;

public class ObjectReferenceServiceCall {

	public Map<String, List<Map<String, Object>>> findReferences(ObjectReferenceServiceInput input, AWSLambda lambda) {
		Map<String, List<Map<String, Object>>> device = new HashMap<String, List<Map<String, Object>>>();
		ObjectReferenceService service = LambdaInvokerFactory.builder().lambdaClient(lambda)
				.build(ObjectReferenceService.class);

		if (Constants.envType.equalsIgnoreCase("Dev"))
			device = (Map<String, List<Map<String, Object>>>) service.findReferencesDev(input);
		if (Constants.envType.equalsIgnoreCase("Develop"))
			device = (Map<String, List<Map<String, Object>>>) service.findReferencesDevelop(input);
		if (Constants.envType.equalsIgnoreCase("Stage"))
			device = (Map<String, List<Map<String, Object>>>) service.findReferencesStage(input);
		if (Constants.envType.equalsIgnoreCase("Prod"))
			device = (Map<String, List<Map<String, Object>>>) service.findReferencesProd(input);

		return device;
	}

}
