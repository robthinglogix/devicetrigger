package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.thinglogix.Constants;

public class PostDeviceServiceCall {
	public Map<String, Object> postDevice(PostDeviceLambdaInput input, AWSLambda lambda) {
		Map<String, Object> device = new HashMap<String, Object>();
		PostDeviceService service = LambdaInvokerFactory.builder().lambdaClient(lambda).build(PostDeviceService.class);

		if (Constants.envType.equalsIgnoreCase("Dev"))
			device = (Map<String, Object>) service.postDeviceDev(input);
		if (Constants.envType.equalsIgnoreCase("Develop"))
			device = (Map<String, Object>) service.postDeviceDevelop(input);
		if (Constants.envType.equalsIgnoreCase("Stage"))
			device = (Map<String, Object>) service.postDeviceStage(input);
		if (Constants.envType.equalsIgnoreCase("Prod"))
			device = (Map<String, Object>) service.postDeviceProd(input);

		return device;
	}
}
