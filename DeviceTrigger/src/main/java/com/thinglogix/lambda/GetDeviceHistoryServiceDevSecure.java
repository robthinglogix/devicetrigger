package com.thinglogix.lambda;

import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface GetDeviceHistoryServiceDevSecure {
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_HISTORY)
	List<Map<String, Object>> getDeviceHistoryDev(GetDeviceHistoryDevSecureLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_HISTORY_DEVELOP)
	List<Map<String, Object>> getDeviceHistoryDevelop(GetDeviceHistoryDevSecureLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_HISTORY_STAGE)
	List<Map<String, Object>> getDeviceHistoryStage(GetDeviceHistoryDevSecureLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICE_HISTORY_PROD)
	List<Map<String, Object>> getDeviceHistoryProd(GetDeviceHistoryDevSecureLambdaInput input);
}
