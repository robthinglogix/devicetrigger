package com.thinglogix.lambda;

import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.invoke.LambdaFunction;

public interface GetDevicesByTypeService {
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICES_BY_TYPE)
	List<Map<String, Object>> getDeviceTypeDev(GetDevicesByTypeLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICES_BY_TYPE_DEVELOP)
	List<Map<String, Object>> getDeviceTypeDevelop(GetDevicesByTypeLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICES_BY_TYPE_STAGE)
	List<Map<String, Object>> getDeviceTypeStage(GetDevicesByTypeLambdaInput input);
	
	@LambdaFunction(functionName=LambdaFunctionNames.GET_DEVICES_BY_TYPE_PROD)
	List<Map<String, Object>> getDeviceTypeProd(GetDevicesByTypeLambdaInput input);
}
