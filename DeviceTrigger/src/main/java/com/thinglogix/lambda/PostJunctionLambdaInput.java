package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

public class PostJunctionLambdaInput {
	private Map<String, Object> body;
	private boolean invoke = true;
	
	//private boolean isParent;
	

	public Map<String, Object> getbody() {
		return body;
	}
	
	public boolean isInvoke() {
		return invoke;
	}

	public void setInvoke(boolean invoke) {
		this.invoke = invoke;
	}
	
	public void setParentId(String parentId) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("parentId", parentId);
	}
	
	public String getParentId() {
		if(body == null) {
			return null;
		}
		return (String) body.get("parentId");
	}
	
	
	public void setDeviceId(String deviceId) {
		if(body == null) {
			body = new HashMap<String, Object>();
		}
		body.put("deviceId", deviceId);
	}
	
	public String getDeviceId() {
		if(body == null) {
			return null;
		}
		return (String) body.get("deviceId");
	}
	
	
}
