package com.thinglogix.lambda;

import java.util.HashMap;
import java.util.Map;

public class GetDeviceLambdaInput {
	private Map<String, String> params;
	private Map<String, Object> query;

	public Map<String, Object> getQuery() {
		return query;
	}

	public void setQuery(Map<String, Object> query) {
		this.query = query;
	}

	
	public GetDeviceLambdaInput() {
		super();
		this.query = new HashMap<String, Object>();
		query.put("withGroups", "false");
	}

	/*"query":{
	     "withGroups":false
	 },*/

	private boolean invoke = true;
	public boolean isInvoke() {
		return invoke;
	}

	public void setInvoke(boolean invoke) {
		this.invoke = invoke;
	}

	public Map<String, String> getParams() {
		return params;
	}
	
	public void setDeviceId(String deviceId) {
		if(params == null) {
			params = new HashMap<String, String>();
		}
		params.put("device_id", deviceId);
	}
	
	public void setWithGroups(Boolean withGroups) {
		query.put("withGroups", String.valueOf(withGroups));
	}
	
	public String getDeviceId() {
		if(params == null) {
			return null;
		}
		return params.get("device_id");
	}
}
