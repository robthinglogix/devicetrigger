package com.thinglogix.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.thinglogix.Constants;

public class SearchServiceCall {
	public List<Map<String, Object>> searchDevice(SearchServiceInput input, AWSLambda lambda) {
		List<Map<String, Object>> device = new ArrayList<Map<String, Object>>();
		SearchService service = LambdaInvokerFactory.builder().lambdaClient(lambda).build(SearchService.class);

		if (Constants.envType.equalsIgnoreCase("Dev"))
			device = (List<Map<String, Object>>) service.searchDeviceDev(input);
		if (Constants.envType.equalsIgnoreCase("Develop"))
			device = (List<Map<String, Object>>) service.searchDeviceDevelop(input);
		if (Constants.envType.equalsIgnoreCase("Stage"))
			device = (List<Map<String, Object>>) service.searchDeviceStage(input);
		if (Constants.envType.equalsIgnoreCase("Prod"))
			device = (List<Map<String, Object>>) service.searchDeviceProd(input);

		return device;
	}
}
