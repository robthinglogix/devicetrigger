package com.thinglogix.trigger;

public class ExpressionEvaluationException extends Throwable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 896748943263519794L;

	public ExpressionEvaluationException() {
		super();
	}

	public ExpressionEvaluationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ExpressionEvaluationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExpressionEvaluationException(String message) {
		super(message);
	}

	public ExpressionEvaluationException(Throwable cause) {
		super(cause);
	}
	
}
