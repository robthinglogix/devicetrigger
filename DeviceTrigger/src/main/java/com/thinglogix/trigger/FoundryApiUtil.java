package com.thinglogix.trigger;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.thinglogix.lambda.GetAccountDetailsInput;
import com.thinglogix.lambda.GetAccountDetailsServiceCall;
import com.thinglogix.lambda.GetDeviceChildrenInput;
import com.thinglogix.lambda.GetDeviceChildrenServiceCall;
import com.thinglogix.lambda.GetDeviceLambdaInput;
import com.thinglogix.lambda.GetDeviceParentsInput;
import com.thinglogix.lambda.GetDeviceParentsServiceCall;
import com.thinglogix.lambda.GetDeviceServiceCall;
import com.thinglogix.lambda.GetDeviceTypeLambdaInput;
import com.thinglogix.lambda.GetDeviceTypeServiceCall;
import com.thinglogix.lambda.GetUserDetailsInput;
import com.thinglogix.lambda.GetUserDetailsServiceCall;

public class FoundryApiUtil {

//	private static Map<String, Object> deviceDetailCache = new HashMap<>();
	
	public static Map<String, Object> getAccountDetail(String account_id) {

		try {
			Map<String, Object> account = new HashMap<String, Object>();

			AWSLambda lambda = AWSLambdaClientBuilder.standard().withRegion(DeviceTriggerFromSNS.region).build();

			GetAccountDetailsServiceCall service = new GetAccountDetailsServiceCall();
			
			GetAccountDetailsInput input = new GetAccountDetailsInput();
			input.setAccountId(account_id);
			input.setQuery(Collections.<String, String>emptyMap());
			account = (Map<String, Object>) service.getAccountDetails(input, lambda);

			return account;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	public static List<Map<String,Object>> getDeviceParentsDetail(String objectId){
		try {

			AWSLambda lambda = AWSLambdaClientBuilder.standard().withRegion(DeviceTriggerFromSNS.region).build();

			/*GetDeviceParentsService service = LambdaInvokerFactory.build(GetDeviceParentsService.class, lambda);*/
			GetDeviceParentsServiceCall service=new GetDeviceParentsServiceCall();
			GetDeviceParentsInput input = new GetDeviceParentsInput();
			input.setObjectId(objectId);
			//System.out.println(service.getDeviceParents(input));
			List<Map<String,Object>> obj = service.getDeviceParents(input,lambda);
			return obj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Map<String,Object>> getDeviceChildrenDetail(String objectId){
		try {

			AWSLambda lambda = AWSLambdaClientBuilder.standard().withRegion(DeviceTriggerFromSNS.region).build();
			GetDeviceChildrenServiceCall service=new GetDeviceChildrenServiceCall();
			GetDeviceChildrenInput input = new GetDeviceChildrenInput();
			input.setObjectId(objectId);
			//System.out.println(service.getDeviceParents(input));
			List<Map<String,Object>> obj = service.getDeviceChildren(input, lambda);
			return obj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Map<String, Object> getUserDetails(String user_id) {
		try {
			
			Map<String, Object> user = new HashMap<String, Object>();

			AWSLambda lambda = AWSLambdaClientBuilder.standard().withRegion(DeviceTriggerFromSNS.region).build();

			GetUserDetailsServiceCall service = new GetUserDetailsServiceCall();
			
			GetUserDetailsInput input = new GetUserDetailsInput();
			input.setUserId(user_id);
			input.setWithObjects(false);
			user = (Map<String, Object>) service.getUserDetails(input, lambda);
			user.remove("password");
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public static Map<String, Object> getDeviceTypeDetail(String deviceTypeId) {
		try {
			Map<String, Object> deviceType = new HashMap<String, Object>();

			AWSLambda lambda = AWSLambdaClientBuilder.standard().withRegion(DeviceTriggerFromSNS.region).build();

			/*GetDeviceTypeService service = LambdaInvokerFactory.build(GetDeviceTypeService.class, lambda);*/
			GetDeviceTypeServiceCall service = new GetDeviceTypeServiceCall();
			
			GetDeviceTypeLambdaInput input = new GetDeviceTypeLambdaInput();
			input.setTypeId(deviceTypeId);
		//	System.out.println(service.getDeviceType(input,lambda));

			deviceType = (Map<String, Object>) service.getDeviceType(input,lambda);

			return deviceType;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Map<String, Object> getDeviceDetail(String deviceId) {
		try {
			Map<String, Object> device = null;
//			Object cahedObject = deviceDetailCache.get(deviceId);
//			if (cahedObject != null) {
//				device = (Map<String, Object>) cahedObject;
//			} else {
				device = new HashMap<String, Object>();

				AWSLambda lambda = AWSLambdaClientBuilder.standard().withRegion(DeviceTriggerFromSNS.region).build();

			//	GetDeviceService service = LambdaInvokerFactory.builder().lambdaClient(lambda)
					//	.build(GetDeviceService.class);

				GetDeviceServiceCall service = new GetDeviceServiceCall();
				
				// GetDeviceService service =
				// LambdaInvokerFactory.build(GetDeviceService.class, lambda);
				GetDeviceLambdaInput input = new GetDeviceLambdaInput();
				input.setDeviceId(deviceId);
				

					device = (Map<String, Object>) service.getDevice(input,lambda);


//				if (device != null) {
//					deviceDetailCache.put(deviceId, device);
//				}
//			}
			return device;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void main(String[] args) {
		// getDeviceTypeDetail("97c6ddf0-9500-11e6-990b-e120f3f7d66d");

		// String requestBody = "{" + "\"id\": \" \"," + "\"name\": \"" +
		// "subMenuName" + "\"," + "\"menu_item\": \""
		// + "mainMenuItem" + "\"," + "\"url\":\" " + "pageUrl" + "\"," +
		// "\"user_role\": \"Admin\","
		// + "\"account_id\":\"" + "accountID" + "\"" + "}";
		//
		// System.out.println(requestBody);
		System.out.println(getUserDetails("b29a8b80-51c5-11e8-b89a-9bd28f9d606d"));

	}

}
