package com.thinglogix.trigger;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.dynamodbv2.model.StreamRecord;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent.DynamodbStreamRecord;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thinglogix.Constants;

import org.apache.commons.lang3.StringUtils;

public class DeviceTriggerFromSNS implements RequestHandler<SNSEvent, Object> {
    public static Boolean emailDebugEnabled;
    public static Set<String> debugEnabledDeviceIds = new HashSet<>();
    public static StringBuilder debugInfo;
    public static LambdaLogger logger;

    private static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.defaultClient();
    private static DynamoDB dynamoDB = new DynamoDB(client);
    private static AmazonSNS snsClient = AmazonSNSClientBuilder.defaultClient();


    public static String region;
    public static String dynamodbEventName = null;

    private static String objectId = null;
    private static String accountId = null;
    private static String typeId = null;

    private static Map<String, Object> account = null;
    private static Map<String, Object> userRefObj = null;
    private static Map<String, Object> typeObj = null;

    private Map<String, Object> deviceType = null;

    private static Map<String, AttributeValue> newRecord;

    private static Map<String, AttributeValue> oldRecord;

    private static Map<String, Object> normalizedNewRecord;

    private static Map<String, Object> normalizedOldRecord;

    public static Map<String, Object> getNormalizedNewRecord() {
        return normalizedNewRecord;
    }

    public static Map<String, Object> getNormalizedOldRecord() {
        return normalizedOldRecord;
    }

    public static void setNormalizedNewRecord(Map<String, Object> normalizedNewRecord) {
        DeviceTriggerFromSNS.normalizedNewRecord = normalizedNewRecord;
    }

    public static void setNormalizedOldRecord(Map<String, Object> normalizedOldRecord) {
        DeviceTriggerFromSNS.normalizedOldRecord = normalizedOldRecord;
    }

    public static Map<String, AttributeValue> getNewRecord() {
        return newRecord;
    }

    public static void setNewRecord(Map<String, AttributeValue> newRecord) {
        DeviceTriggerFromSNS.newRecord = newRecord;
    }

    public static Map<String, AttributeValue> getOldRecord() {
        return oldRecord;
    }

    public static void setOldRecord(Map<String, AttributeValue> oldRecord) {
        DeviceTriggerFromSNS.oldRecord = oldRecord;
    }

    public static String getObjectId() {
        return objectId;
    }

    public static String getAccountId() {
        return accountId;
    }

    public static void setObjectId(String objectId) {
        DeviceTriggerFromSNS.objectId = objectId;
    }

    @Override
    public Object handleRequest(SNSEvent input, Context context) {

        context.getLogger().log("===============EXECUTION STARTED==============");

        try {
            Constants.envType = System.getenv("ENV");
        } catch (Exception e) {

        }
        if (Constants.envType == null) {
            Constants.envType = System.getProperty("ENV");
        }

        try {
            Constants.sfdc_client_id = System.getenv("SFDC_CLIENT_ID");
        } catch (Exception e) {

        }
        if (Constants.sfdc_client_id == null) {
            Constants.sfdc_client_id = System.getProperty("SFDC_CLIENT_ID");
        }

        try {
            Constants.sfdc_client_secret = System.getenv("SFDC_CLIENT_SECRET");
        } catch (Exception e) {

        }
        if (Constants.sfdc_client_secret == null) {
            Constants.sfdc_client_secret = System.getProperty("SFDC_CLIENT_SECRET");
        }

        logger = context.getLogger();
        debugInfo = new StringBuilder();
        debugEnabledDeviceIds = new HashSet<>();
        try {
            String debug = System.getenv("EMAIL_DEBUG_ENABLED");
            emailDebugEnabled = "true".equalsIgnoreCase(debug);
        } catch (Exception e) {
            emailDebugEnabled = false;
        }
        if (emailDebugEnabled) {
            for (int i = 1;; i++) {
                try {
                    String debugDeviceId = System.getenv("DEBUG_DEVICE_" + i);
                    if (!StringUtils.isEmpty(debugDeviceId)) {
                        debugEnabledDeviceIds.add(debugDeviceId);
                    } else {
                        break;
                    }
                } catch (Exception e) {
                    break;
                }
            }
        }

        try {
            String snsMessage = input.getRecords().get(0).getSNS().getMessage();
            DynamodbStreamRecord vDynamodbStreamRecord;

            ObjectMapper mapper = new ObjectMapper();
            mapper.setSerializationInclusion(Include.NON_NULL);
            vDynamodbStreamRecord = mapper.reader().forType(DynamodbStreamRecord.class).readValue(snsMessage);

            String msg = mapper.writeValueAsString(vDynamodbStreamRecord);
            System.out.println(msg);
            addDebugInfoNewLine("Event Name: " + vDynamodbStreamRecord.getEventName());
            addDebugInfoNewLine("Event Source: " + vDynamodbStreamRecord.getEventSource());
            addDebugInfoNewLine("ARNe: " + vDynamodbStreamRecord.getEventSourceARN());
            String dynamoDbTableName = null;
            dynamoDbTableName = getDynamoDbTableName(vDynamodbStreamRecord.getEventSourceARN());
            Table table = dynamoDB.getTable(dynamoDbTableName);

            StreamRecord vStreamRecord = vDynamodbStreamRecord.getDynamodb();
            newRecord = vStreamRecord.getNewImage();
            normalizedNewRecord = DynamoDBConverter.toSimpleMapValue(newRecord);
            oldRecord = vStreamRecord.getOldImage();
            normalizedOldRecord = DynamoDBConverter.toSimpleMapValue(oldRecord);

            if (newRecord.containsKey("deviceId")) {
                objectId = newRecord.get("deviceId").getS();
                userRefObj = null;

            }
            if (newRecord.containsKey("accountId")) {
                accountId = newRecord.get("accountId").getS();
                account = null;
            }
            if (newRecord.containsKey("typeId")) {
                typeId = newRecord.get("typeId").getS();
                typeObj = null;
            }

            addDebugInfoNewLine("============================START PROCESSING================================");
            addDebugInfoNewLine("ACCOUNT ID = " + accountId);
            addDebugInfoNewLine("DEVICE TYPE ID = " + typeId);
            addDebugInfoNewLine("OBJECT ID = " + objectId);
            processFormulaFields(context, table, vDynamodbStreamRecord, newRecord, oldRecord, objectId, accountId,
                    typeId);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        sendDebugLog();
        return null;
    }

    private void sendDebugLog() {
        if (emailDebugEnabled && debugEnabledDeviceIds.size() > 0 && debugInfo.length() > 0) {
            addDebugInfo("START SENDING DEBUG INFO");
            try {
                String snsTopicArn = System.getenv("SNS_TOPIC_ARN");
                addDebugInfo("SNS_TOPIC_ARN = " + snsTopicArn);
                publishToSNS(snsTopicArn, debugInfo.toString());
            } catch (Exception e) {
                logger.log("ERROR DURING SNS PUBLISH");
                e.printStackTrace();
            }
        }
    }

    private static void publishToSNS(String snsTopicArn, String debugInfo) {
        String regionName = System.getenv("AWS_DEFAULT_REGION");
        addDebugInfo("AWS REGION FOR DEBUG SNS - " + regionName);
        PublishRequest publishRequest = new PublishRequest(snsTopicArn, debugInfo);
        PublishResult publishResult = snsClient.publish(publishRequest);
        System.out.println("Debug info SNS message Id - " +
        publishResult.getMessageId());
    }

    public static void addDebugInfoNewLine(String str) {
        logger.log(str);
        if (emailDebugEnabled && debugEnabledDeviceIds.contains(objectId)) {
            debugInfo.append(str).append("\n");
        }
    }

    public static void addDebugInfo(String str) {
        logger.log(str);
        if (emailDebugEnabled && debugEnabledDeviceIds.contains(objectId)) {
            debugInfo.append(str);
        }
    }

    public static void addDebugInfo(Throwable t) {
        logger.log(t.getMessage());

        if (emailDebugEnabled && debugEnabledDeviceIds.contains(objectId)) {
            debugInfo.append(t.getMessage()).append("\n");
            for (StackTraceElement e : t.getStackTrace()) {
                debugInfo.append(e.toString()).append("\n");
            }
        }
    }

    /**
     * Extract the DynamoDB table name from ARN string
     * 
     * @param eventSourceARN
     * @return
     */
    private String getDynamoDbTableName(String eventSourceARN) {
        String[] arr = eventSourceARN.split(":");
        arr = arr[5].split("/");

        return arr[1];
    }

    @SuppressWarnings("unchecked")
    private void processFormulaFields(Context context, Table table, DynamodbStreamRecord vDynamodbStreamRecord,
            Map<String, AttributeValue> newRecord, Map<String, AttributeValue> oldRecord, String objectId,
            String accountId, String typeId) {
        try {
            String oldGlobalActionRunTime = oldRecord.get("a.global_action_datetime_of_last_run").getS();
            String newGlobalActionRunTime = newRecord.get("a.global_action_datetime_of_last_run").getS();

            if (newGlobalActionRunTime != null && !newGlobalActionRunTime.equals(oldGlobalActionRunTime)) {
                return;
            }

        } catch (Exception e) {

        }

        deviceType = FoundryApiUtil.getDeviceTypeDetail(typeId);

        String eventName = vDynamodbStreamRecord.getEventName();
        dynamodbEventName = eventName;
        if ("MODIFY".equals(eventName) || "INSERT".equals(eventName) || "REMOVE".equals(eventName)) {
            Map<String, Object> updatedProertiesMap = new LinkedHashMap<>();

            LinkedHashMap<String, Object> formulaFieldsMap = new LinkedHashMap<>();
            for (Entry<String, Object> entry : deviceType.entrySet()) {
                if (!entry.getKey().startsWith("a.")) {
                    continue;
                }
                Map<String, Object> field = (Map<String, Object>) entry.getValue();
                if (field != null && "Formula".equals(field.get("data_type"))) {
                    String formula = (String) field.get("formula");
                    addDebugInfoNewLine("FORMULA FIELD -- " + entry.getKey() + " = " + formula);
                    Object executeFormulaOnlyOnInsert = field.get("executeFormulaOnlyOnInsert");
                    Object executeFormulaOnDelete = field.get("executeFormulaOnDelete");
                    if (new Boolean(true).equals(executeFormulaOnlyOnInsert) && "MODIFY".equals(eventName)) { // The
                                                                                                              // formula
                                                                                                              // should
                                                                                                              // be
                                                                                                              // executed
                                                                                                              // on
                                                                                                              // INSERT
                                                                                                              // only
                        addDebugInfo("MODIFY EVENT FOR INSERT ONLY FORMULA, SKIPPING");
                        continue;
                    }
                    if (StringUtils.isEmpty(formula) || (formula.contains("$OLD") && "INSERT".equals(eventName))) {
                        addDebugInfo("EMPTY FORMULA OR INSERT WITH $OLD, SKIPPING");
                        continue;
                    }
                    if (new Boolean(false).equals(executeFormulaOnDelete) && "REMOVE".equals(eventName)) {
                        addDebugInfo("DELETE FORMULA, SKIPPING");
                        continue;
                    }

                    formulaFieldsMap.put(entry.getKey(), entry.getValue());
                }
            }

            addDebugInfoNewLine("NUMBER OF FORMULA FIELDS BEING PROCESSED - " + formulaFieldsMap.size());
            addDebugInfoNewLine(formulaFieldsMap.toString());

            Boolean changeInFormulaFieldValue = false;
            changeInFormulaFieldValue = detectChangeInFormulaField(newRecord, oldRecord, formulaFieldsMap);
            if (changeInFormulaFieldValue) { // Value of formula field. No need to proceed
                return;
            }

            for (Entry<String, Object> entry : formulaFieldsMap.entrySet()) {
                if (!entry.getKey().startsWith("a.")) {
                    continue;
                }
                Map<String, Object> field = (Map<String, Object>) entry.getValue();
                if (field != null && "Formula".equals(field.get("data_type"))) {
                    try {
                        String fieldName = entry.getKey();
                        addDebugInfoNewLine("FORMULA FIELD NAME:-" + fieldName);

                        Object computedValue = null;
                        String formula = (String) field.get("formula");
                        addDebugInfoNewLine("formula BEFORE replacing literals");
                        addDebugInfoNewLine("FORMULA: " + fieldName + " = " + formula);

                        formula = replaceFieldsWithLiterals(entry.getKey(), formula, newRecord, oldRecord,
                                formulaFieldsMap);
                        addDebugInfoNewLine("formula AFTER replacing literals");
                        addDebugInfoNewLine("FORMULA: " + fieldName + " = " + formula);

                        formula = formula.replace("'", "\\'");
                        computedValue = new FormulaEvaluator().evaluate(formula);

                        addDebugInfoNewLine("COMPUTATION DONE");
                        addDebugInfoNewLine("FORMULA VALUE " + fieldName + " = " + computedValue);

                        if (!formula.contains("executeAction")) {
                            updatedProertiesMap.put(fieldName, computedValue);
                        }
                    } catch (Exception | ExpressionEvaluationException e) {
                        addDebugInfo(e);
                    }
                }
            }

            // Update dynamodb table here
            addDebugInfoNewLine("UPDATE DYNAMODB TABLE-----");
            String updateExpression;
            String setExpression = "set ";
            String removeExpression = "remove ";

            int j = 0;
            int sets = 0;
            int removes = 0;
            for (Entry<String, Object> updateFields : updatedProertiesMap.entrySet()) {
                addDebugInfoNewLine(updateFields.getKey());
                if (updateFields.getValue() != null && updateFields.getValue().toString().length() > 0) {
                    addDebugInfoNewLine("Set");
                    Object attr = deviceType.get(updateFields.getKey());
                    String dataType = (String) ((Map<String, Object>) attr).get("data_type");
                    if (dataType.equals("Formula")) {
                        dataType = (String) ((Map<String, Object>) attr).get("formulaType");
                    }
                    if (dataType.equals("Counter")) {
                        setExpression += ("#k" + (j) + " = if_not_exists(#k" + (j) + ", :start" + (j) + ") + :v" + (j) + ", ");
                    } else {
                        setExpression += ("#k" + (j) + " = :v" + (j) + ", ");
                    }
                    sets++;
                } else {
                    addDebugInfoNewLine("Remove");
                    removeExpression += ("#k" + (j)  + ", ");
                    removes++;
                }
                j++;
            }

            // for(int i = 0; i < updatedProertiesMap.size(); i++) {
            // updateExpression += ("#k" + (i) + " =:v" + (i) + ", ");
            // }
            int i = setExpression.lastIndexOf(",");
            if (i != -1) {
                setExpression = setExpression.substring(0, i);
            }
            int k = removeExpression.lastIndexOf(",");
            if (k != -1) {
                removeExpression = removeExpression.substring(0, k);
            }
            addDebugInfoNewLine("set expression: " + setExpression + " Sets:" + sets);
            addDebugInfoNewLine("remove expression: " + removeExpression + " Removes:" + removes);
            if (sets != 0 && removes != 0) {
                updateExpression = setExpression + " " + removeExpression;
            }  else if (sets != 0) {
                updateExpression = setExpression;
            } else if (removes != 0) {
                updateExpression = removeExpression;
            } else {
                addDebugInfoNewLine("No updates.");
                return;
            }

            addDebugInfoNewLine("update expression: " + updateExpression);

            int index = 0;
            ValueMap valueMap = new ValueMap();
            NameMap nameMap = new NameMap();

            for (Entry<String, Object> updateFields : updatedProertiesMap.entrySet()) {
                addDebugInfoNewLine("" + index + " - " + updateFields.getValue());
                nameMap = nameMap.with("#k" + (index), updateFields.getKey());
                addDebugInfoNewLine("KEY - " + updateFields.getKey());
                addDebugInfoNewLine("VALUE - " + updateFields.getValue());
                if (updateFields.getValue() == null || updateFields.getValue().toString().length() <     1) {
                    addDebugInfoNewLine("VALUE IS NULL");
                    index++;
                    continue;
                }
                Object attr = deviceType.get(updateFields.getKey());
                String dataType = (String) ((Map<String, Object>) attr).get("data_type");
                if (dataType.equals("Formula")) {
                    dataType = (String) ((Map<String, Object>) attr).get("formulaType");
                }
                if (dataType.equals("Counter")) {
                    valueMap = valueMap.withNumber(":start" + (index), 0);
                }

                if ("Decimal".equals(dataType) || "Integer".equals(dataType) || "Counter".equals(dataType) || "Currency".equals(dataType)) {
                    Object value = updateFields.getValue();

                    if (value instanceof Integer) {
                        valueMap = valueMap.withNumber(":v" + index, (Integer) value);
                    } else if (value instanceof Double) {
                        valueMap = valueMap.withNumber(":v" + index, (Double) value);
                    } else if (value instanceof Byte) {
                        valueMap = valueMap.withNumber(":v" + index, ((Byte) updateFields.getValue()).doubleValue());
                    } else {
                        valueMap = valueMap.withString(":v" + index, String.valueOf(updateFields.getValue()));
                    }
                    
                } else {
                    valueMap = valueMap.withString(":v" + index, String.valueOf(updateFields.getValue()));
                }

                index++;
                addDebugInfoNewLine("update Field: " + updateFields.getKey() + " = " + updateFields.getValue());
            }

            // this covers sets and removes
            if (sets != 0) {
                UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("deviceId", objectId)
                        .withUpdateExpression(updateExpression).withNameMap(nameMap).withValueMap(valueMap)
                        .withReturnValues(ReturnValue.NONE);
                table.updateItem(updateItemSpec);
            } else if (removes != 0) {
                UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("deviceId", objectId)
                        .withUpdateExpression(updateExpression).withNameMap(nameMap)
                        .withReturnValues(ReturnValue.NONE);
                table.updateItem(updateItemSpec);
            } 

        }

    }

    private Boolean detectChangeInFormulaField(Map<String, AttributeValue> newRecord,
            Map<String, AttributeValue> oldRecord, LinkedHashMap<String, Object> formulaFieldsMap) {
        Boolean changeInFormulaFieldValue = false;
        for (Entry<String, Object> entry : formulaFieldsMap.entrySet()) {
            Map<String, Object> field = (Map<String, Object>) entry.getValue();
            if (field != null && "Formula".equals(field.get("data_type"))) {
                String fieldName = entry.getKey();
                addDebugInfoNewLine("FORMULA FIELD NAME:-" + fieldName);
                String oldValue = null;
                try {
                    oldValue = oldRecord.get(fieldName).getS();

                    if (oldValue == null) {
                        oldValue = String.valueOf(oldRecord.get(fieldName).getN());
                    }
                } catch (Exception e) {
                }
                addDebugInfoNewLine("OLD VALUE OF FIELD " + fieldName + " = " + oldValue);
                String newValue = null;
                try {
                    newValue = newRecord.get(fieldName).getS();

                    if (newValue == null) {
                        newValue = String.valueOf(newRecord.get(fieldName).getN());
                    }
                } catch (Exception e) {
                }
                addDebugInfoNewLine("NEW VALUE OF FIELD " + fieldName + " = " + newValue);
                // Check if there is a change in the value of formula
                // field.
                // If there is a change, we will NOT proceed computation
                // of formula field.
                // This is to stop the infinite triggering of this code.
                if (oldValue != null) {
                    if (!oldValue.equals(newValue)) {
                        addDebugInfoNewLine(
                                "This trigger was executed due to formula fields computation, no need to move further");
                        changeInFormulaFieldValue = true;
                    }
                }
            }
        }
        return changeInFormulaFieldValue;
    }

    private String replaceFieldsWithLiterals(String formulaFieldName, String formula,
            Map<String, AttributeValue> newRecord, Map<String, AttributeValue> oldRecord,
            LinkedHashMap<String, Object> formulaFieldsMap) throws Exception {

        addDebugInfoNewLine("ENTERING RECURSIVE FUNCTIONS FOR RESOLVING FIELDS WITH THEIR VALUES");

        formula = formula.replaceAll("[$][{]accountId[}]", accountId);
        formula = formula.replaceAll("[$][{]typeId[}]", typeId);
        formula = formula.replaceAll("[$][{]deviceId[}]", objectId);

        String tempFormulaFieldName = formulaFieldName;
        while (formula.contains("${") || formula.contains("$OLD{")) {

            //// This block handles all variables that need replacement with new value
            addDebugInfoNewLine("--START RESOLVING NEW VALLUES--");
            Pattern pattern = Pattern.compile("[$][{](.*?)[}]");
            Matcher matcher = pattern.matcher(formula);
            Map<String, String> matcherMap = new HashMap<>();
            while (matcher.find()) {
                String value;
                String field = matcher.group(1);
                if (!tempFormulaFieldName.equals("a." + field) && formulaFieldsMap.containsKey("a." + field) && !((Map<String, Object>) (formulaFieldsMap.get("a." + field))).get("formulaType").equals("Counter")) {
                    value = "##FORMULA##"
                            + (String) ((Map<String, Object>) formulaFieldsMap.get("a." + field)).get("formula");
                } else {
                    String tempFieldName = field;
                    String referencedFieldName = null;
                    if (field.contains("__r.")) {
                        addDebugInfoNewLine("REFERENCE FIELD");
                        String arr[] = field.split("__r.");
                        tempFieldName = arr[0];
                        referencedFieldName = arr[1];
                        addDebugInfoNewLine(tempFieldName + "__r." + referencedFieldName);
                    }
                    String jsonKey;
                    if ("accountId".equals(tempFieldName)) {
                        jsonKey = "accountId";
                    } else if ("typeId".equals(tempFieldName)) {
                        jsonKey = "typeId";
                    } else if ("Created".equals(tempFieldName)) {
                        jsonKey = "Created";
                    } else if ("Updated".equals(tempFieldName)) {
                        jsonKey = "Updated";
                    } else if ("created_by".equals(tempFieldName)) {
                        jsonKey = "created_by";
                    } else if ("updated_by".equals(tempFieldName)) {
                        jsonKey = "updated_by";
                    } else if ("parentId".equals(tempFieldName)) {
                        jsonKey = "parentId";
                    } else {
                        jsonKey = "a." + tempFieldName;
                    }
                    AttributeValue av = newRecord.get(jsonKey);
                    if (av == null) {
                        addDebugInfoNewLine("av is null");
                        Object attr = deviceType.get(jsonKey);
                        if (attr == null) {
                            if (parentGroupContains(objectId, jsonKey)) {
                                value = getValueFromParentGroup(objectId, jsonKey);
                            } else {
                                addDebugInfoNewLine("Field with name " + jsonKey + " does not exist in the record");
                                value = null;
                            }
                        } else {
                            String dataType = (String) ((Map<String, Object>) attr).get("data_type");
                            if (dataType.equals("Formula")) {
                                dataType = (String) ((Map<String, Object>) attr).get("formulaType");
                            }
                            if ("Decimal".equals(dataType) || "Integer".equals(dataType) || "Counter".equals(dataType) || "Currency".equals(dataType)) {
                                value = "0";
                            } else {
                                value = "";
                            }

                        }

                        // value = null;
                    } else {
                        addDebugInfoNewLine("av is NOT null");
                        value = av.getS();
                        if (value == null) {
                            addDebugInfoNewLine("av getS is null");
                            value = av.getN();
                            if (value == null) {
                                addDebugInfoNewLine("av getN is null");
                                value = av.getBOOL().toString();
                                if (value == null) {
                                    value = av.toString();
                                }
                            }
                        }
                        addDebugInfoNewLine("value is - " + value);
                        if (field.contains("__r.")) {
                            addDebugInfo("REFERENCE OBJECT ID = " + value);
                            if (value != null) {

                                if ("accountId".equals(jsonKey) || "a.account_ref".equals(jsonKey)) {
                                    if (account == null) {
                                        account = FoundryApiUtil.getAccountDetail(value);
                                        addDebugInfo("ACCOUNT OBJECT = " + account);
                                        addDebugInfo("ACCOUNT FIELD NAME = " + referencedFieldName);
                                    }
                                    if (account != null) {
                                        if (account.containsKey(referencedFieldName)) {
                                            value = account.get(referencedFieldName).toString();
                                            addDebugInfo("ACCOUNT FIELD VALUE = " + value);
                                        } else {
                                            value = "";
                                        }
                                    }
                                } else if ("typeId".equals(jsonKey)) {
                                    if (typeObj == null) {
                                        typeObj = FoundryApiUtil.getDeviceTypeDetail(value);
                                    }
                                    if (typeObj != null) {
                                        if (typeObj.containsKey("a." + referencedFieldName)) {
                                            value = typeObj.get("a." + referencedFieldName).toString();
                                        } else if (typeObj.containsKey(referencedFieldName)) {
                                            value = typeObj.get(referencedFieldName).toString();
                                        } else {
                                            value = "";
                                        }
                                    }
                                } else if ("a.user_ref".equals(jsonKey) || "updated_by".equals(jsonKey)
                                        || "created_by".equals(jsonKey)) {
                                    if (userRefObj == null) {
                                        userRefObj = FoundryApiUtil.getUserDetails(value);
                                        addDebugInfo("USER OBJECT = " + userRefObj);
                                        addDebugInfo("USER FIELD NAME = " + referencedFieldName);
                                    }
                                    if (userRefObj != null) {
                                        if (userRefObj.containsKey(referencedFieldName)) {
                                            value = userRefObj.get(referencedFieldName).toString();
                                        } else {
                                            value = "";
                                        }
                                    }
                                } else {
                                    Map<String, Object> device = FoundryApiUtil.getDeviceDetail(value);
                                    addDebugInfo("REFERENCE OBJECT =  " + device);
                                    if (device != null) {
                                        if (device.containsKey("a." + referencedFieldName)) {
                                            Object tempValue = device.get("a." + referencedFieldName);
                                            if (tempValue != null) {
                                                value = tempValue.toString();
                                                addDebugInfo("REFERENCE OBJECT CONTAINS ATTRIBUTE  " + value);
                                            } else {
                                                value = null;
                                                addDebugInfo("REFERENCE OBJECT CONTAINS NULL ATTRIBUTE");
                                            }
                                        }  else if (device.containsKey(referencedFieldName)) {
                                            value = device.get(referencedFieldName).toString();
                                            addDebugInfo("REFERENCE OBJECT CONTAINS SYSTEM ATTRIBUTE  " + value);
                                        } else {
                                            value = "";
                                            addDebugInfo("REFERENCE OBJECT DOES NOT CONTAIN ATTRIBUTE  " + value);
                                        }
                                    } else {
                                        value = null;
                                        addDebugInfo("REFERENCE OBJECT IS NULL");
                                    }
                                }
                            }
                        }

                        if (tempFormulaFieldName.equals("a." + field)) {
                            if (StringUtils.isEmpty(value.trim())) {
                                value = "0";
                            }
                        }
                    }
                }
                matcherMap.put(field, value);
            }
            for (Map.Entry<String, String> entry : matcherMap.entrySet()) {
                String value = entry.getValue() != null ? entry.getValue() : "";
                if (value != null && value.startsWith("##FORMULA##")) {
                    value = value.replace("##FORMULA##", "");
                    // formula = formula.replace("${" + entry.getKey() + "}", value);
                    addDebugInfoNewLine("START RESOLVE INNER FORMULA FIELD:" + entry.getKey() + " = " + value);
                    String innerFormula = "(" + replaceFieldsWithLiterals("a." + entry.getKey(), value, newRecord,
                            oldRecord, formulaFieldsMap) + ")";
                    addDebugInfoNewLine("END RESOLVE INNER FORMULA FIELD:" + entry.getKey());
                    formula = formula.replace("${" + entry.getKey() + "}", innerFormula);
                } else {
                    formula = formula.replaceAll("[$][{]" + entry.getKey() + "[}]", value);
                }
            }
            addDebugInfoNewLine("--END RESOLVING NEW VALLUES--");
            //// This block handles all variables that need replacement with old values
            addDebugInfoNewLine("--START RESOLVING OLD VALLUES--");
            pattern = Pattern.compile("[$]OLD[{](.*?)[}]");
            matcher = pattern.matcher(formula);
            matcherMap = new HashMap<>();
            while (matcher.find()) {
                String value;
                String field = matcher.group(1);

                String tempFieldName = field;
                String referencedFieldName = null;
                if (field.contains("__r.")) {
                    String arr[] = field.split("__r.");
                    tempFieldName = arr[0];
                    referencedFieldName = arr[1];
                }

                String jsonKey;
                if ("accountId".equals(tempFieldName)) {
                    jsonKey = "accountId";
                } else if ("typeId".equals(tempFieldName)) {
                    jsonKey = "typeId";
                } else if ("Created".equals(tempFieldName)) {
                    jsonKey = "Created";
                } else if ("Updated".equals(tempFieldName)) {
                    jsonKey = "Updated";
                } else if ("created_by".equals(tempFieldName)) {
                    jsonKey = "created_by";
                } else if ("updated_by".equals(tempFieldName)) {
                    jsonKey = "updated_by";
                } else if ("parentId".equals(tempFieldName)) {
                    jsonKey = "parentId";
                } else {
                    jsonKey = "a." + tempFieldName;
                }
                addDebugInfo("OLD JSON KEY" + jsonKey);

                AttributeValue av = oldRecord.get(jsonKey);
                if (av == null) {
                    throw new Exception("Field with name " + jsonKey + " does not exist in the record");
                }

                addDebugInfoNewLine("av is NOT null");
                value = av.getS();
                if (value == null) {
                    addDebugInfoNewLine("av getS is null");
                    value = av.getN();
                    if (value == null) {
                        addDebugInfoNewLine("av getN is null");
                        value = av.getBOOL().toString();
                        if (value == null) {
                            value = av.toString();
                        }
                    }
                }
                addDebugInfoNewLine("value is - " + value);

                addDebugInfo("OLD FIELD " + field);
                addDebugInfo("OLD VALUE " + value);
                if (field.contains("__r.")) {
                    if (value != null) {
                        Map<String, Object> device = FoundryApiUtil.getDeviceDetail(value);
                        addDebugInfo("OLD REFERENCE OBJECT =  " + device);
                        if (device != null) {
                            addDebugInfo("REFERENCED FIELD NAME " + referencedFieldName);
                            if (device.containsKey("a." + referencedFieldName)) {
                                Object tempValue = device.get("a." + referencedFieldName);
                                if (tempValue != null) {
                                    value = tempValue.toString();
                                    addDebugInfo("REFERENCE OBJECT CONTAINS ATTRIBUTE  " + value);
                                } else {
                                    value = null;
                                    addDebugInfo("REFERENCE OBJECT CONTAINS NULL ATTRIBUTE");
                                }
                            } else if (device.containsKey(referencedFieldName)) {
                                value = device.get(referencedFieldName).toString();
                                addDebugInfo("REFERENCE SYSTEM ATTRIBUTE VALUE " + value);
                            } else {
                                addDebugInfo("REFERENCED FIELD NAME NOT FOUND");
                                value = "";
                            }
                        } else {
                            value = null;
                        }
                    }
                }

                if (tempFormulaFieldName.equals("a." + field)) {
                    if (StringUtils.isEmpty(value.trim())) {
                        value = "0";
                    }
                }

                matcherMap.put(field, value);
            }
            for (Map.Entry<String, String> entry : matcherMap.entrySet()) {
                String value = entry.getValue();
                formula = formula.replaceAll("[$]OLD[{]" + entry.getKey() + "[}]", value);
            }
            addDebugInfoNewLine("--END RESOLVING OLD VALLUES--");
        }
        return formula;
    }

    private boolean parentGroupContains(String objectId, String jsonKey) {
        boolean found = false;
        List<Map<String, Object>> deviceParents = FoundryApiUtil.getDeviceParentsDetail(objectId);
        if (deviceParents.size() > 0) {
            for (Map<String, Object> d : deviceParents) {
                if (d.containsKey("parentId")) {
                    Map<String, Object> device = FoundryApiUtil.getDeviceDetail(d.get("parentId").toString());
                    if (device.containsKey(jsonKey)) {
                        found = true;
                        break;
                    }
                }
            }
        }
        return found;
    }

    private String getValueFromParentGroup(String objectId, String jsonKey) {
        String value = "";
        List<Map<String, Object>> deviceParents = FoundryApiUtil.getDeviceParentsDetail(objectId);
        if (deviceParents.size() > 0) {
            for (Map<String, Object> d : deviceParents) {
                if (d.containsKey("parentId")) {
                    Map<String, Object> device = FoundryApiUtil.getDeviceDetail(d.get("parentId").toString());
                    if (device.containsKey(jsonKey)) {
                        value = device.get(jsonKey).toString();
                        break;
                    }
                }
            }
        }

        return value;
    }
}
