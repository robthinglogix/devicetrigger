package com.thinglogix.trigger;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.amazonaws.services.lambda.AWSLambdaAsync;
import com.amazonaws.services.lambda.AWSLambdaAsyncClientBuilder;
import com.amazonaws.services.lambda.model.InvocationType;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thinglogix.Constants;
import com.thinglogix.lambda.LambdaFunctionNames;

public class GlobalActionHelper {
	
	private String actionId;
	
	public GlobalActionHelper(String actionId) {
		super();
		this.actionId = actionId;
	}
	
	public void invoke() {
		String actionId = this.actionId;
		Map<String, Object> actionDetail = FoundryApiUtil.getDeviceDetail(actionId);
		if (actionDetail != null) {
			System.out.println(actionDetail);
			invoke(actionId);
		}
	}

	private void invoke(String actionId) {
		String objectId = DeviceTriggerFromSNS.getObjectId();
		String finalPayload = null;
		Map<String, Object> normalizedNewRecord = DeviceTriggerFromSNS.getNormalizedNewRecord();
		Map<String, Object> normalizedOldRecord = DeviceTriggerFromSNS.getNormalizedOldRecord();
		ObjectMapper objectMapper = new ObjectMapper();

//		System.out.println("IN INVOKE ACTION\n " + 
//							"OBJECT ID = " + objectId + "\n "+
//							"ACTION ID = " + actionId);
		
		if(actionId == null || actionId.trim().length() == 0) {
			return;
		}
		
		 AWSLambdaAsync lambdaAsync = AWSLambdaAsyncClientBuilder.standard()
					.withRegion(System.getenv("AWS_DEFAULT_REGION"))
					.build();
		 Map<String, Object> bodyMap = new HashMap<String, Object>();
		 Map<String, Object> finalMap = new HashMap<String, Object>();
		 bodyMap.put("actionId", actionId);
		 bodyMap.put("objectId", objectId);
		 if (normalizedNewRecord != null && !normalizedNewRecord.isEmpty()) {
			 bodyMap.put("object", normalizedNewRecord);
		 }
		 if (normalizedOldRecord != null && !normalizedOldRecord.isEmpty()) {
			 bodyMap.put("oldObject", normalizedOldRecord);
		 }
		 finalMap.put("body", bodyMap);
		 finalMap.put("invoke", true);
		 try {
			 finalPayload = objectMapper.writeValueAsString(finalMap);
		 } catch(JsonProcessingException e) {
			 e.printStackTrace();
		 }
		 String postActionCall = null;
		if (Constants.envType.equalsIgnoreCase("Dev")){
			postActionCall = LambdaFunctionNames.POST_ACTION_CALL;
		}	
		
		if (Constants.envType.equalsIgnoreCase("Develop")){
			postActionCall = LambdaFunctionNames.POST_ACTION_CALL_DEVELOP;
		}	
		if (Constants.envType.equalsIgnoreCase("Stage")){
			postActionCall = LambdaFunctionNames.POST_ACTION_CALL_STAGE;
		}
		if (Constants.envType.equalsIgnoreCase("Prod")){
			postActionCall = LambdaFunctionNames.POST_ACTION_CALL_PROD;
		}
		InvokeRequest invokeRequest = new InvokeRequest().withFunctionName(postActionCall);
//		 System.out.println("dataPayload : "+dataPayload);
		 invokeRequest.withPayload(finalPayload).withInvocationType(InvocationType.Event);
		 lambdaAsync.invoke(invokeRequest);
	}
	
	public static void main(String [] args) {
		String payloadTemplate = "com.test/[name]";
		
		String payload = (String) payloadTemplate;
		Pattern pattern = Pattern.compile("[\\[](.*?)[\\]]");
		Matcher matcher = pattern.matcher((CharSequence) payloadTemplate);
		
		while (matcher.find()) {
			String field = matcher.group(1);
			payload = payload.replace("[" + field + "]", "xyz");
		}
		System.out.println(payload);
	}
	
}
