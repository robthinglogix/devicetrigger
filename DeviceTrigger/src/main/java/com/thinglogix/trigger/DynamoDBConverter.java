package com.thinglogix.trigger;

import static com.amazonaws.util.BinaryUtils.copyAllBytesFrom;

import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

public class DynamoDBConverter {
	public static <T> Map<String, T> toSimpleMapValue(
            Map<String, AttributeValue> values) {
        if (values == null) {
            return null;
        }

        Map<String, T> result = new LinkedHashMap<String, T>();
        for (Map.Entry<String, AttributeValue> entry : values.entrySet()) {
            T t = toSimpleValue(entry.getValue());
            result.put(entry.getKey(), t);
        }
        return result;
    }
	

    public static List<Object> toSimpleList(List<AttributeValue> attrValues) {
        if (attrValues == null)
            return null;
        List<Object> result = new ArrayList<Object>(attrValues.size());
        for (AttributeValue attrValue : attrValues) {
            Object value = toSimpleValue(attrValue);
            result.add(value);
        }
        return result;
    }

    public static <T> T toSimpleValue(AttributeValue value) {
    	if (value == null) {
            return null;
        }
        if (Boolean.TRUE.equals(value.getNULL())) {
            return null;
        } else if (Boolean.FALSE.equals(value.getNULL())) {
            throw new UnsupportedOperationException("False-NULL is not supported in DynamoDB");
        } else if (value.getBOOL() != null) {
            @SuppressWarnings("unchecked")
            T t = (T) value.getBOOL();
            return t;
        } else if (value.getS() != null) {
            @SuppressWarnings("unchecked")
            T t = (T) value.getS();
            return t;
        } else if (value.getN() != null) {
            @SuppressWarnings("unchecked")
            T t = (T) new BigDecimal(value.getN());
            return t;
        } else if (value.getB() != null) {
            @SuppressWarnings("unchecked")
            T t = (T) copyAllBytesFrom(value.getB());
            return t;
        } else if (value.getSS() != null) {
            @SuppressWarnings("unchecked")
            T t = (T) new LinkedHashSet<String>(value.getSS());
            return t;
        } else if (value.getNS() != null) {
            Set<BigDecimal> set = new LinkedHashSet<BigDecimal>(value.getNS().size());
            for (String s : value.getNS()) {
                set.add(new BigDecimal(s));
            }
            @SuppressWarnings("unchecked")
            T t = (T) set;
            return t;
        } else if (value.getBS() != null) {
            Set<byte[]> set = new LinkedHashSet<byte[]>(value.getBS().size());
            for (ByteBuffer bb : value.getBS()) {
                set.add(copyAllBytesFrom(bb));
            }
            @SuppressWarnings("unchecked")
            T t = (T) set;
            return t;
        } else if (value.getL() != null) {
            @SuppressWarnings("unchecked")
            T t = (T) toSimpleList(value.getL());
            return t;
        } else if (value.getM() != null) {
            @SuppressWarnings("unchecked")
            T t = (T) toSimpleMapValue(value.getM());
            return t;
        } else {
            throw new IllegalArgumentException(
                    "Attribute value must not be empty: " + value);
        }

    }

}
