package com.thinglogix.trigger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.TimeZone;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;

public class FoundryFormulaFunctions {
	public static String concat(Object a, Object b) {
		return a.toString().concat(b.toString());
	}

	public static String concat(Object... a) {
		String result = "";
		for (Object obj : a) {
			result = result.concat(obj.toString());
		}
		return result;
	}

	public static String toString(byte a) {
		return "" + a;
	}

	public static String toString(Object a) {
		return "" + a.toString();
	}

	public static String toString(int a) {
		return "" + a;
	}

	public static String toString(float a) {
		return "" + a;
	}

	public static String toString(double a) {
		return "" + a;
	}

	public static String toString(long a) {
		return "" + a;
	}

	public static String timestamp_to_datetime(long ts, String format, String timeZone) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
		return dateFormat.format(new java.util.Date(ts));
	}

	public static String timestamp_to_datetime(long ts, String format) {
		return new SimpleDateFormat(format).format(new java.util.Date(ts));
	}

	public static String timestamp_to_datetime(long ts) {
		return timestamp_to_datetime(ts, "MM/dd/yyyy HH:mm:ss");
	}

	public static String epoch_to_datetime(long ts, String format, String timeZone) {
		return timestamp_to_datetime(ts * 1000, format, timeZone);
	}

	public static String epoch_to_datetime(long ts, String format) {
		return timestamp_to_datetime(ts * 1000, format);
	}

	public static String epoch_to_datetime(long ts) {
		return epoch_to_datetime(ts, "MM/dd/yyyy HH:mm:ss");
	}

	public static String epoch_to_datetime(int ts, String format) {
		return epoch_to_datetime(Long.valueOf(ts), format);
	}

	public static String epoch_to_datetime(int ts) {
		return epoch_to_datetime(ts, "MM/dd/yyyy HH:mm:ss");
	}

	public static String executeAction(String actionId) {
		System.out.println("Execute action id: " + actionId);
		if (actionId != null && actionId.trim().length() > 0) {
			System.out.println("In execute action: actionId = " + actionId + "DEVICE ID is -" + DeviceTriggerFromSNS.getObjectId());
			new GlobalActionHelper(actionId).invoke();
		}
		return "Success - " + new Date();
	}

	public static String base64Encode(String str) {
		return new String(Base64.getEncoder().encode(str.getBytes()));
	}

	public static String base64Decode(String base64Str) {
		return new String(Base64.getDecoder().decode(base64Str.getBytes()));
	}

	public static Boolean equals(String a, String b) {
		return a.equals(b);
	}

	public static Object IF(Boolean decider, Object arg1, Object arg2) {
		if (decider) {
			return arg1;
		} else {
			return arg2;
		}
	}

	public static void main(String[] args) {
		System.out.println(Math.pow(10, 1000 / 400.0));
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	public static boolean startsWith(String firstString, String subString) {

		return firstString.startsWith(subString);
	}

	public static boolean endsWith(String firstString, String subString) {

		return firstString.endsWith(subString);
	}

	public static boolean contains(String firstString, String subString) {

		return firstString.contains(subString);
	}

	public static String subString(String str, int beginIndex, int endIndex) {
		try {
			return str.substring(beginIndex, endIndex);
		} catch (Exception e) {
			return "";
		}
	}

	public static String subString(String str, int beginIndex) {
		try {
			return str.substring(beginIndex);
		} catch (Exception e) {
			return "";
		}
	}

	public static boolean equalsIgnoreCase(String firstString, String secondString) {

		return firstString.equalsIgnoreCase(secondString);
	}

	public static String toLower(String firstString) {

		return firstString.toLowerCase();
	}

	public static String toUpper(String firstString) {

		return firstString.toUpperCase();
	}

	public static Long now() {
		return System.currentTimeMillis();
	}

	public static String today(String format, String timeZone) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
		return dateFormat.format(new java.util.Date(now()));
	}

	public static String today(String format) {
		return new SimpleDateFormat(format).format(new java.util.Date(now()));
	}

	public static String today() {
		return today("MM/dd/yyyy");
	}

	public static boolean isEmpty() {
		return true;
	}

	public static boolean isEmpty(String a) {
		if (a == null) {
			return true;
		} else {
			return a.length() < 1;
		}
	}

	public static boolean isEmpty(Object a) {
		return a == null;
	}
	
	public static boolean isEmpty(byte a) {
		return false;
	}
	
	public static boolean isEmpty(short a) {
		return false;
	}
	
	public static boolean isEmpty(int a) {
		return false;
	}
	
	public static boolean isEmpty(long a) {
		return false;
	}
	
	public static boolean isEmpty(Boolean a) {
		return a == null;
	}
	
	public static boolean isEmpty(float a) {
		return false;
	}
	
	public static boolean isEmpty(double a) {
		return false;
	}
	
	public static boolean isNotEmpty(byte a) {
		return true;
	}
	
	public static boolean isNotEmpty(short a) {
		return true;
	}
	
	public static boolean isNotEmpty(int a) {
		return true;
	}
	
	public static boolean isNotEmpty(long a) {
		return true;
	}
	
	public static boolean isNotEmpty(float a) {
		return true;
	}
	
	public static boolean isNotEmpty(double a) {
		return true;
	}
	
	public static boolean isEmpty(Number a) {
		return a == null;
	}

	public static boolean isNotEmpty() {
		return !isEmpty();
	}

	public static boolean isNotEmpty(String a) {
		return !isNotEmpty(a);
	}

	public static boolean isNotEmpty(Object a) {
		return !isEmpty(a);
	}
	
	public static boolean isNotEmpty(Number a) {
		return !isEmpty(a);
	}

	public static boolean isChanged(String fieldName) {
		String oldValue;
		String newValue;
		AttributeValue av;

		av = DeviceTriggerFromSNS.getNewRecord().get("a." + fieldName);
        if (av == null) {
		    av = DeviceTriggerFromSNS.getNewRecord().get(fieldName);
        }

        if (av == null) {
            newValue = "";
        } else {
            if("INSERT".equals(DeviceTriggerFromSNS.dynamodbEventName)) {
                return true;
            }
            newValue = av.getS();
            if (newValue == null) {
                newValue = av.getN();
                if (newValue == null) {
                    newValue = av.getBOOL().toString();
                    if (newValue == null) {
                        newValue = av.toString();
                    }
                }
            }
        }

		Object o = DeviceTriggerFromSNS.getOldRecord();
		if (o == null) {
			oldValue = "";
		} else {
			av = DeviceTriggerFromSNS.getOldRecord().get("a." + fieldName);
            if (av == null) {
			    av = DeviceTriggerFromSNS.getOldRecord().get(fieldName);
            }
			if (av == null) {
				oldValue = "";
			} else {
                oldValue = av.getS();
                if (oldValue == null) {
                    oldValue = av.getN();
                    if (oldValue == null) {
                        oldValue = av.getBOOL().toString();
                        if (oldValue == null) {
                            oldValue = av.toString();
                        }
                    }
                }
            }
		}
		return !newValue.equals(oldValue);
	}
    
}
