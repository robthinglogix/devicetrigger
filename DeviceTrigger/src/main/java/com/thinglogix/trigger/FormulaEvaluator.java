package com.thinglogix.trigger;

import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptException;

import gnu.jel.CompilationException;
import gnu.jel.CompiledExpression;
import gnu.jel.Evaluator;
import gnu.jel.Library;

public class FormulaEvaluator {
	
	
	private Library lib;
	
	public FormulaEvaluator() {
		Class[] staticLib = new Class[2];
		try {
			staticLib[0] = Class.forName("java.lang.Math");
			staticLib[1] = Class.forName("com.thinglogix.trigger.FoundryFormulaFunctions");
		} catch (ClassNotFoundException e) {
			// Can't be ;)) ...... in java ... ;)
		}
		lib = new Library(staticLib, null, null, null, null);
		
	}
	
	
	public Object evaluate(String expression) throws ExpressionEvaluationException {
		try {
			lib.markStateDependent("random", null);
			// Compile
			CompiledExpression expr_c = null;
			expr_c = Evaluator.compile(expression, lib);
			if (expr_c != null) {

				// Evaluate (Can do it now any number of times FAST !!!)
				Object result = null;
				try {
					result = expr_c.evaluate(null);
					DeviceTriggerFromSNS.addDebugInfoNewLine(result.toString());
					return result;
				} catch (Throwable e) {
					DeviceTriggerFromSNS.addDebugInfoNewLine("Exception emerged from JEL compiled" + " code (IT'S OK) :");
					DeviceTriggerFromSNS.addDebugInfo(e);
					throw new ExpressionEvaluationException(e);
				}
			}
			return null;
		} catch (CompilationException ce) {
			DeviceTriggerFromSNS.addDebugInfo("–––COMPILATION ERROR :");
			DeviceTriggerFromSNS.addDebugInfoNewLine(ce.getMessage());
			DeviceTriggerFromSNS.addDebugInfoNewLine(expression);
			
			int column = ce.getColumn(); // Column, where error was found
			DeviceTriggerFromSNS.addDebugInfoNewLine("Column: " + column);
			throw new ExpressionEvaluationException(ce);
		}
	}
	
	
	public Boolean evaluateFlag(String expression) throws ExpressionEvaluationException {
		Boolean resultFlag = true;
		try {
			lib.markStateDependent("random", null);
			// Compile
			CompiledExpression expr_c = null;
			expr_c = Evaluator.compile(expression, lib);
			if (expr_c != null) {

				// Evaluate (Can do it now any number of times FAST !!!)
				Object result = null;
					try {
						result = expr_c.evaluate(null);
					} catch (Throwable e) {
						resultFlag = false;
						e.printStackTrace();
					}
					DeviceTriggerFromSNS.addDebugInfoNewLine(result.toString());
				
			}

		} catch (Exception ce) {
			resultFlag = false;
			//throw new ExpressionEvaluationException(ce);
		}
		return resultFlag;
	}
	
	public static void main(String args[]) throws ExpressionEvaluationException {
		String expression = "\"111A,GD,,Collin's Home,Williamsburg,MA\"";
		expression = expression.replace("'", "\\'");
		new FormulaEvaluator().evaluate(expression);
	}
	

	public static void main2(String args[]) {
		// Assemble the expression
//		String expr = "round(10.0/(1+2))";
//		String expr = "concat(toString(1), toString(2))";
//		String expr = "executeAction(equals(\"None\",\"None\") ? \"4d0089e0-e589-11e6-9a3b-6fa81684cb2a\" : \"\")";
		
		String expr = "1 == 2 ? printA() : printB()";
		expr = expr.replaceAll("[$][{](.*?)[}]", "1");

		// Set up the library
		Class[] staticLib = new Class[2];
		try {
			staticLib[0] = Class.forName("java.lang.Math");
			staticLib[1] = Class.forName("com.thinglogix.trigger.FoundryFormulaFunctions");
		} catch (ClassNotFoundException e) {
			// Can't be ;)) ...... in java ... ;)
		}
		Library lib = new Library(staticLib, null, null, null, null);
		try {
			lib.markStateDependent("random", null);
			// Compile
			CompiledExpression expr_c = null;
			expr_c = Evaluator.compile(expr, lib);
			if (expr_c != null) {

				// Evaluate (Can do it now any number of times FAST !!!)
				Object result = null;
				try {
					result = expr_c.evaluate(null);
					System.out.println(result);
				} catch (Throwable e) {
					System.err.println("Exception emerged from JEL compiled" + " code (IT'S OK) :");
					System.err.print(e);
				}
			}
		} catch (CompilationException ce) {
			System.err.print("–––COMPILATION ERROR :");
			System.err.println(ce.getMessage());
			System.err.print("                       ");
			System.err.println(expr);
			int column = ce.getColumn(); // Column, where error was found
			for (int i = 0; i < column + 23 - 1; i++)
				System.err.print(' ');
			System.err.println('^');
		}

	}

	public static void main1(String args[]) throws ScriptException {
		// ScriptEngineManager mgr = new ScriptEngineManager();
		// ScriptEngine engine = mgr.getEngineByName("JavaScript");
		// String foo = "\"a\"+\"a\"";
		// System.out.println(engine.eval(foo));

		String formula = "$a+$b";

		Integer indexOfDollar = null;
		Integer currentIndex = 0;
		List<String> variables = new ArrayList<String>();

		while ((indexOfDollar = formula.indexOf("$", currentIndex)) != -1) {
			if (formula.charAt(indexOfDollar + 1) != '$') {
				StringBuffer buffer = new StringBuffer();
				char currentChar;
				for (currentIndex = indexOfDollar + 1; currentIndex < formula.length()
						&& Character.isLetterOrDigit(currentChar = formula.charAt(currentIndex)); currentIndex++) {
					buffer.append(currentChar);
				}
				if (buffer.length() > 0) {
					variables.add(buffer.toString());
				}
				indexOfDollar = currentIndex;
			} else {
				indexOfDollar = currentIndex;
			}
		}

		for (String variable : variables) {
			formula = formula.replace('$' + variable, "2");
		}
		System.out.println(formula);

	}
}
