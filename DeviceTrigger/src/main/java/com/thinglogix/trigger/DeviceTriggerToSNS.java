package com.thinglogix.trigger;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent.DynamodbStreamRecord;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DeviceTriggerToSNS implements RequestHandler<DynamodbEvent, Object> {
    private static AmazonSNS snsClient = AmazonSNSClientBuilder.defaultClient();

	@Override
	public Object handleRequest(DynamodbEvent input, Context context) {

		String topicArn = null;
		String region = System.getenv("AWS_DEFAULT_REGION");
		String env = System.getenv("ENV");

		for (DynamodbStreamRecord vDynamodbStreamRecord : input.getRecords()) {
			if(topicArn == null) {
				String sourceARN = vDynamodbStreamRecord.getEventSourceARN();
				String [] arnArr = sourceARN.split(":");
				String awsAccountId = arnArr[4];
				topicArn = "arn:aws:sns:" + region + ":" + awsAccountId + ":fndy-device-trigger-" + env;
			}
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Include.NON_NULL);
				String msg = mapper.writeValueAsString(vDynamodbStreamRecord);
				PublishRequest publishRequest = new PublishRequest(topicArn, msg);
				PublishResult publishResult = snsClient.publish(publishRequest);
				System.out.println("MessageId - " + publishResult.getMessageId());
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

		}
		return null;
	}

}
