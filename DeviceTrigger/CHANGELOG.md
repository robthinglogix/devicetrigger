# Change Log
## 4.5
### Bug Fixes
- **Old References** referencing $OLD references now works 
### Features
- **Invalid References** delete formula attributes that have invalid references

## 4.4

### Bug Fixes
- **isChanged**: fix null pointer causing function to not evaluate 
- **isEmpty**: isEmpty now works properly for strings 

### Features
- **Counter**: add counter formula type
- **Global Action**: send full old/new object record to global action lambda
- **Java 11**: update to Java version 11 
- **Parent Id**: support new `parentId` attribute to trigger off group changes
- **File Cleanup**: remove un-used files 
- **Number Support**: improve support of different numerical types
- **isChanged**: add support for system attributes
- **Reference Lookup**: add support for looking up system attributes via reference
- **Formula Types**: use formula type when processing formulas that contain other formulas

